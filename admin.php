<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/adminInterface.css" media="all"/>
        <title></title>
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
        $annee = 2015;
        $cptCheckFerie = 0;

	/*Fonction qui vont contenir les jours fériés des base*/
	
        function paques($annee) {
            $tab[1] = $jour_paques = date("j", easter_date($annee));
            $tab[0] = $mois_paques = date("n", easter_date($annee));
            if (strlen($tab[0]) == 1) {
                $tab[0] = "0" . $tab[0];
            }if (strlen($tab[1]) == 1) {
                $tab[1] = "0" . $tab[1];
            }
            return $annee . "-" . $tab[0] . "-" . $tab[1];
        }

        function lundi_paques($annee) {
            $tab[1] = $jour_paques = date("j", mktime(0, 0, 0, 3, 22 + easter_days($annee), $annee));
            $tab[0] = $mois_paques = date("n", mktime(0, 0, 0, 3, 22 + easter_days($annee), $annee));
            if (strlen($tab[0]) == 1) {
                $tab[0] = "0" . $tab[0];
            }if (strlen($tab[1]) == 1) {
                $tab[1] = "0" . $tab[1];
            }
            return $annee . "-" . $tab[0] . "-" . $tab[1];
        }

        function jeudi_ascension($annee) {
            $tab[1] = $jour_ascension = date("j", mktime(0, 0, 0, 3, 60 + easter_days($annee), $annee));
            $tab[0] = $mois_ascension = date("n", mktime(0, 0, 0, 3, 60 + easter_days($annee), $annee));
            if (strlen($tab[0]) == 1) {
                $tab[0] = "0" . $tab[0];
            }if (strlen($tab[1]) == 1) {
                $tab[1] = "0" . $tab[1];
            }
            return $annee . "-" . $tab[0] . "-" . $tab[1];
        }

        function pantecote($annee) {
            $tab[1] = $jour_pantecote = date("j", mktime(0, 0, 0, 3, 71 + easter_days($annee), $annee));
            $tab[0] = $mois_pantecote = date("n", mktime(0, 0, 0, 3, 71 + easter_days($annee), $annee));
            return $annee . "-" . $tab[0] . "-" . $tab[1];
        }

        function vendredi_saint($annee) {
            $tab[1] = $jour_vendredi_saint = date("j", mktime(0, 0, 0, 3, 19 + easter_days($annee), $annee));
            $tab[0] = $mois_vendredi_saint = date("n", mktime(0, 0, 0, 3, 19 + easter_days($annee), $annee));
            return $annee . "-" . $tab[0] . "-" . $tab[1];
        }
	
	// Tableau qui va contenir les dates des jours férié de bases

        $tabJourFerieBase[0] = $annee . "-01-01";
        $tabJourFerieBase[1] = $annee . "-01-02";
        $tabJourFerieBase[2] = vendredi_saint($annee);
        $tabJourFerieBase[3] = lundi_paques($annee);
        $tabJourFerieBase[4] = jeudi_ascension($annee);
        $tabJourFerieBase[5] = pantecote($annee);
        $tabJourFerieBase[6] = $annee . "-08-01";
        $tabJourFerieBase[7] = $annee . "-12-25";


        $tabFerie = $cmd->getJourFerie();

	// Boucle qui va contrôle si la premier valeur du tableau est présente dans la base de données 
        foreach ($tabFerie as $key => $value) {
            if ($value['date_jfe'] != $tabJourFerieBase[0]) {
                
            } else {
                $cptCheckFerie++;
            }
        }

	// Insertion des jours fériés
        if ($cptCheckFerie == 0) {
            for ($i = 0; $i < count($tabJourFerieBase); $i++) {
                $insert = $cmd->addJourFerie($tabJourFerieBase[$i], 1, 1);
            }
        }

        echo"<div id='admin'>";
        echo"<div id='entreprise'>";
        echo"<div id='text'>";
        echo"DONNÉES DE L'ENTREPSIRSE";
        echo"</div>";
        echo"<div id='textTravail'>TEMPS A EFFECTUER PAR JOUR";
        echo"</div>";
        // Formulaire qui va appeller la page qui gère le nombre d'heure par jour que l'employé doit faire
        echo"<div id='jourTravail'>";
        echo"<form action='jourTravailJournalier.php' method='post' />";
        echo"<input type='submit' class='btn' name='tempsTravailJour' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";

        // Formulaire qui va appeller la page qui gère les jours feriées de l'entreprise
        echo"<div id='textFerie'>CHOIX DES JOURS FERIES";
        echo"</div>";
        echo"<div id='jourFerie'>";
        echo"<form action='jourFerie.php' method='post' />";
        echo"<input type='submit' class='btn' name='jourFerie' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";
        // Formulaire qui vas appeller la page qui gère les jours de fermeture de l'entreprise
        echo"<div id='textFermeture'>CHOIX DES JOURS DE RATTRAPAGES";
        echo"</div>";
        echo"<div id='jourFermeture'>";
        echo"<form action='jourFermeture.php' method='post' />";
        echo"<input type='submit' class='btn' name='ajouterFermeture' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";
        //Formulaire qui vas appeller la page qui gère les jours de rattrapages de l'entreprise
        echo"<div id='textRattrapage'>CHOIX DES JOURS DE FERMERTURES";
        echo"</div>";
        echo"<div id='jourRattrapage'> ";
        echo"<form action='jourRattrapage.php' method='post' />";
        echo"<input type='submit' class='btn' name='ajouterRattrapage' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";

        echo"<div id='deconnexion'>";
        echo "<form method='post' action='login.php'>";
        echo"<input type='submit' class='btnDeco' name='deconnexion'  value='DECONNEXION' />";
        echo"</form>";
        echo"</div>";
        echo"</div>";
        echo"<div id='employe'>";



        echo"<div id='text2'>";
        echo"EMPLOYÉ DE L'ENTREPRISE";
        echo"</div>";
        echo"<div id='textNouveau'>NOUVEAU EMPLOYE";
        echo"</div>";

        // Formulaire qui vas appeller la page qui gère la création de nouveau employé
        echo"<div id='nouveauEmp'>";
        echo"<form action='nouveauEmploye.php' method='post' />";
        echo"<input type='submit' class='btn' name='nouveauEmploye' value='CREER'/>";
        echo"</form>";
        echo"</div>";
        // Formulaire qui vas appeller la page qui gère la modification des employés
        echo"<div id='textModifier'>MODIFIER EMPLOYE";
        echo"</div>";
        echo"<div id='modifierEmp'>";
        echo"<form action='modifierEmploye.php' method='post' />";
        echo"<input type='submit' class='btn' name='modifierEmploye' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";
        // Formulaire qui vas appeller la page qui gère la période de travail d'un employé (pourcentage + jours de vacances)
        echo"<div id='textPeriode'>PERIODE";
        echo"</div>";
        echo"<div id='periodeEmp'>";
        echo"<form action='periodeEmploye.php' method='post' />";
        echo"<input type='submit' class='btn' name='periodeEmploye' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";
        // Formulaire qui vas appeller la page qui gère les vacacnces des employés
        echo"<div id='textVacances'>VACACANCES EMPLOYE";
        echo"</div>";
        echo"<div id='vacancesEmp'>";
        echo"<form action='vacancesEmploye.php' method='post' />";
        echo"<input type='submit' class='btn' name='vacancesEmploye' value='MODIFIER'/>";
        echo"</form>";
        echo"</div>";

        echo"<div id='textCalcul'>RECAPUTILATIF";
        echo"</div>";
        echo"<div id='calculEmp'>";
        echo"<form action='calcul.php' method='post' />";
        echo"<tr><td> <input type='submit' class='btn' name='calcul' value='OUVRIR'/>";
        echo"</form>";
        echo"</div>";

        echo"<div id='impressionEmp'>";
        echo"<form action='impression.php' method='post' />";
        echo"<input type='submit' class='btnDeco' name='impressionAdmin' value='IMPRESSION'/>";
        echo"</form>";
        echo"</div>";


        echo"</div>";
        echo"</div>";
        ?>
    </body>
</html>
