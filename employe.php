<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./CSS/admin.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="./CSS/employe.css" media="all"/>
        <title></title>
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
        $idUser = $cmd->getUserLogin($_SESSION['username']);
        $annee = 2015;
        $cptFalse = 0;
        $date = date("Y-m-d");
        $cptControl = 0;
        $tab_mois_nom = array(1 => "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");

        $userId = $cmd->getUserLogin($_SESSION['username']);
        $user = $cmd->getUser($userId['id_emp']);
        $tabPlage = $cmd->getPlageUser($user['id_emp']);

        if (!empty($tabPlage)) {
            foreach ($tabPlage as $key => $value) {
                $tabDatePlage[$key] = $value['date_pla'];
                $tabTempsPlage[$key] = $value['total_temps_pla'];
            }
        } else {
            $tabDatePlage[0] = "";
            $tabTempsPlage[0] = "";
        }

	// Boucle qui va remplir les SESSION des jours avec leur durée du temps effectuer
        for ($i = 1; $i <= 12; $i++) {
            for ($y = 1; $y <= date("t", mktime(0, 0, 0, $y, 1, $annee)); $y++) {
                if (strlen($i) == 1) {
                    $i = "0" . $i;
                }if (strlen($y) == 1) {
                    $y = "0" . $y;
                }
                $valTempsTotal = 0;
                if ($tabDatePlage[0] == $annee . "-" . $i . "-" . $y && count($tabDatePlage) == 1) {
                    $valTempsTotal+=$tabTempsPlage[0];
                } else {
                    while ($tabDatePlage[0] == $annee . "-" . $i . "-" . $y) {
                        if (count($tabDatePlage) == 1) {
                            $valTempsTotal+=$tabTempsPlage[0];
                            break;
                        } else {
                            unset($tabDatePlage[0]);
                            $tabDatePlage = array_values($tabDatePlage);

                            $valTempsTotal+=$tabTempsPlage[0];
                            unset($tabTempsPlage[0]);
                            $tabTempsPlage = array_values($tabTempsPlage);
                        }
                    }
                }
                $heurePlage = 0;
                $minutePlage = 0;
                while ($valTempsTotal >= 60) {
                    $heurePlage+=1;
                    $valTempsTotal = $valTempsTotal - 60;
                }
                $minutePlage = $valTempsTotal;

                $val = $heurePlage . ":" . $minutePlage;
                $_SESSION[$i . "-" . $y] = $val;
            }
        }

        if (isset($_POST['saisie']) || isset($_POST['supprimerPlage'])) {

	    // Suppresion d'une plage
            if (isset($_POST['supprimerPlage'])) {
                $deletePlage = $cmd->deletePlage($_POST['idPlage']);
                $datePlage = $annee . "-" . $_SESSION['date_champ'];
                $tabPlage = $cmd->getPlageUserDate($user['id_emp'], $datePlage);
                $heurePlage = 0;
                $minutePlage = 0;
                foreach ($tabPlage as $key => $value) {
                    $valTempsTotal+=$value['total_temps_pla'];
                }
                while ($valTempsTotal >= 60) {
                    $heurePlage+=1;
                    $valTempsTotal = $valTempsTotal - 60;
                }
                $minutePlage = $valTempsTotal;

                $_SESSION[$_SESSION['date_champ']] = $heurePlage . ":" . $minutePlage;
            }

            $datePlage = $annee . "-" . $_POST['champSaisie'];
            $tabPlage = $cmd->getPlageUserDate($user['id_emp'], $datePlage);

            echo"<div id='content'>";

            $heurePlageDebut = 0;
            $minutePlageDebut = 0;
            $heurePlageFin = 0;
            $minutePlageFin = 0;
            echo"<div id='titre1'>PLAGE DE TRAVAIL</div>";
            echo"<div id='text1'>DEBUT</div>";
            echo"<div id='text2'>FIN</div>";
            echo"<div id='plage'>";
            echo"<table>";

	    // 
            foreach ($tabPlage as $key => $value) {
                echo"<form action='employe.php' method='post' />";
                $heurePlageDebut = 0;
                $minutePlageDebut = 0;
                $heurePlageFin = 0;
                $minutePlageFin = 0;

                $debut = $value['temps_debut_pla'];
                $fin = $value['temps_fin_pla'];
                while ($debut >= 60) {
                    $heurePlageDebut+=1;
                    $debut = $debut - 60;
                }
                $minutePlageDebut = $debut;
                while ($fin >= 60) {
                    $heurePlageFin+=1;
                    $fin = $fin - 60;
                }
                $minutePlageFin = $fin;

                $totalPlageDebut = $heurePlageDebut . ":" . $minutePlageDebut;
                $totalPlageFin = $heurePlageFin . ":" . $minutePlageFin;
                echo"<tr><td>" . $totalPlageDebut . "</td><td width='100px'></td><td>" . $totalPlageFin . "</td>";
                echo"<input type='hidden' name='idPlage' value=\"" . $value['id_pla'] . "\"/>";
                echo"<input type='hidden' name='champSaisie' value=\"" . $_POST['champSaisie'] . "\"/>";
                echo"<td width='100px'></td><td><input type='submit' name='supprimerPlage' value='X'/></td></tr>";
                echo"</form>";
            }
            echo "</table>";
            echo"</div>";
            echo"<div id='titre2'>AJOUTER UNE PLAGE</div>";
            echo"<div id='text3'>DÉBUT</div>";
            echo"<form action='employe.php' method='post' />";
            echo"<div id='champHeureDebut'><input type='text'  class='input' name='heureDebutPlage' value=''/></div>";
            echo"<div id='champMinuteDebut'><input type='text'  class='input' name='minDebutPlage' value=''/></div>";
            echo"<div id='text4'>FIN</div>";

            echo"<div id='champHeureFin'><input type='text'  class='input' name='heureFinPlage' value=''/></div>";
            echo"<div id='champMinuteFin'><input type='text' class='input' name='minFinPlage' value=''/></div>";
            echo"<div id='btnAjouter'><input type='submit' class='btn' name='creePlage' value='Validée'/></div>";
            echo"</form>";

            echo"<form action='employe.php' method='post' />";
            echo"<div id='btnRetour'><input type='submit' class='btn' name='retour' value='Retour'/></div>";
            echo"</form>";

            $_SESSION['date_champ'] = $_POST['champSaisie'];
            echo "</div>";
        } else {
	    
	    // Création d'une plage
            if (isset($_POST['creePlage'])) {

                $heureDebut = $_POST['heureDebutPlage'];
                $minuteDebut = $_POST['minDebutPlage'];
                $heureFin = $_POST['heureFinPlage'];
                $minuteFin = $_POST['minFinPlage'];
                $datePlage = $annee . "-" . $_SESSION['date_champ'];

		/*Test des données*/
		
                $tabPoint[0] = substr($heureDebut, 0, 1);
                $tabPoint[1] = substr($heureDebut, 1, 1);
                $tabPoint[2] = substr($minuteDebut, 0, 1);
                $tabPoint[3] = substr($minuteDebut, 1, 1);
                $tabPoint[4] = substr($heureFin, 0, 1);
                $tabPoint[5] = substr($heureFin, 1, 1);
                $tabPoint[6] = substr($minuteFin, 0, 1);
                $tabPoint[7] = substr($minuteFin, 1, 1);

                if ($heureDebut == "" || $minuteDebut == "" || $heureFin == "" || $minuteFin == "") {
                    $cptFalse = 1;
                } else if (strlen($heureDebut) > 2 || strlen($minuteDebut) > 2 || strlen($heureFin) > 2 || strlen($minuteFin) > 2) {
                    $cptFalse = 1;
                } else if ($heureDebut > 24 || $minuteDebut > 59 || $heureFin > 24 || $minuteFin > 59) {
                    $cptFalse = 1;
                } else if (!is_numeric($heureDebut) || !is_numeric($minuteDebut) || !is_numeric($heureFin) || !is_numeric($minuteFin)) {
                    $cptFalse = 1;
                } else if ($tabPoint[0] == "." || $tabPoint[1] == "." || $tabPoint[2] == "." || $tabPoint[3] == "." || $tabPoint[4] == "." || $tabPoint[5] == "." || $tabPoint[6] == "." || $tabPoint[7] == ".") {
                    $cptFalse = 1;
                } else if ($heureDebut > $heureFin) {
                    $cptFalse = 1;
                }
                if ($cptFalse == 0) {

		    /*Données Correctes*/
		    
                    $tempsTotalPlage = 0;
                    $tempsDebut = $heureDebut * 60 + $minuteDebut;
                    $tempsFin = $heureFin * 60 + $minuteFin;
                    $tempsTotalPlage = $tempsFin - $tempsDebut;

                    $insert = $cmd->addPlage($tempsDebut, $tempsFin, $tempsTotalPlage, $datePlage, $idUser['id_emp']);

                    $tabPlage = $cmd->getPlageUserDate($idUser['id_emp'], $datePlage);
                    $heurePlage = 0;
                    $minutePlage = 0;
                    foreach ($tabPlage as $key => $value) {
                        $valTempsTotal+=$value['total_temps_pla'];
                    }
                    while ($valTempsTotal >= 60) {
                        $heurePlage+=1;
                        $valTempsTotal = $valTempsTotal - 60;
                    }
                    $minutePlage = $valTempsTotal;

                    $_SESSION[$_SESSION['date_champ']] = $heurePlage . ":" . $minutePlage;
                }
            }

            // Recuperation des données
            $idUser = $cmd->getUserLogin($_SESSION['username']);
            $tabRatrapage = $cmd->getJourRattrapage();
            $tabFerie = $cmd->getJourFerie();
            $tabFermeture = $cmd->getJourFermeture();
            $tabVacances = $cmd->getVacances($idUser['id_emp']);


            // Test si les tableau sont vide
            if (!empty($tabRatrapage)) {
                foreach ($tabRatrapage as $key => $value) {
                    $tabJourRattrapage[$key] = $value['date_jra'];
                }
            } else {
                $tabJourRattrapage[0] = "";
            }
            if (!empty($tabFerie)) {
                foreach ($tabFerie as $key => $value) {
                    $tabJourFerie[$key] = $value['date_jfe'];
                }
            } else {
                $tabJourFerie[0] = "";
            }
            if (!empty($tabFermeture)) {
                foreach ($tabFermeture as $key => $value) {
                    $tabJourFermetureDebut[$key] = $value['date_debut_fer'];
                    $tabJourFermetureFin[$key] = $value['date_fin_fer'];
                }
            } else {
                $tabJourFermetureDebut[0] = "";
                $tabJourFermetureFin[0] = "";
            }
            if (!empty($tabVacances)) {
                foreach ($tabVacances as $key => $value) {
                    $tabJourVacanceDebut[$key] = $value['date_debut_vac'];
                    $tabJourVacanceFin[$key] = $value['date_fin_vac'];
                }
            } else {
                $tabJourVacanceDebut[0] = "";
                $tabJourVacanceFin[0] = "";
            }
            echo "<form method='post' action='impression.php'>";
            echo" <input type='submit' name='impression' value='Impression' />";
            echo"</form>";
            echo "<form method='post' action='login.php'>";
            echo" <input type='submit' name='deconnexion'  value='déconnexion' />";
            echo"</form>";




            // Boucle Création du calendrier
            for ($mois = 1; $mois <= 12; $mois++) {
                echo"<div id='mois_$mois'>";
                echo"<table border='1'>";
                echo"<tr>" . $tab_mois_nom[$mois] . "</tr>";
                for ($jour = 1; $jour <= date("t", mktime(0, 0, 0, $mois, 1, $annee)); $jour++) {
                    $valJour = date("N", mktime(0, 0, 0, $mois, $jour, $annee));
                    // Switch : pour determiner le jour de la semaine
                    switch ($valJour) {
                        case 1:
                            $valJour = "L";
                            break;
                        case 2:
                            $valJour = "M";
                            break;
                        case 3:
                            $valJour = "M";
                            break;
                        case 4:
                            $valJour = "J";
                            break;
                        case 5:
                            $valJour = "V";
                            break;
                        case 6:
                            $valJour = "S";
                            break;
                        case 7:
                            $valJour = "D";
                        default :
                    }

                    if (strlen($mois) == 1) {
                        $moisText = "0" . $mois;
                    } else {
                        $moisText = $mois;
                    }
                    if (strlen($jour) == 1) {
                        $jourText = "0" . $jour;
                    } else {
                        $jourText = $jour;
                    }

                    if ($valJour == "S" || $valJour == "D") {
                        $class = "class='caseWeek'";
                        $cptControl = 1;
                    } else {
                        $class = "";
                    }
                    if ($annee . "-" . $moisText . "-" . $jourText == $tabJourRattrapage[0]) {
                        if ($class == "") {
                            $class = "class='caseJourRattrapage'";
                        } 
                        $nbrIndiceRattrapage = count($tabJourRattrapage);
                        if ($nbrIndiceRattrapage == 1) {
                            
                        } else {
                            //unset() détruit la ou les variables dont le nom a été passé en argument var.
                            unset($tabJourRattrapage[0]);
                            //array_values() retourne les valeurs du tableau array et l'indexe de facon numérique.
                            $tabJourRattrapage = array_values($tabJourRattrapage);
                        }
                        $cptControl = 1;
                    } else if ($annee . "-" . $moisText . "-" . $jourText == $tabJourFerie[0]) {
                        if ($class == "") {
                            $class = "class='caseJourFerie'";
                        } 
                        $nbrIndiceFerie = count($tabJourFerie);
                        if ($nbrIndiceFerie == 1) {
                            
                        } else {
                            unset($tabJourFerie[0]);
                            $tabJourFerie = array_values($tabJourFerie);
                        }
                        $cptControl = 1;
                    } else if ($annee . "-" . $moisText . "-" . $jourText >= $tabJourFermetureDebut[0] && $annee . "-" . $moisText . "-" . $jourText <= $tabJourFermetureFin[0]) {
                        if ($class == "") {
                            $class = "class='caseJourFermeture'";
                        } 
                        if ($annee . "-" . $moisText . "-" . $jourText == $tabJourFermetureFin[0]) {
                            $nbrIndiceFermetureDebut = count($tabJourFermetureDebut);
                            if ($nbrIndiceFermetureDebut == 1) {
                                
                            } else {
                                unset($tabJourFermetureDebut[0]);
                                unset($tabJourFermetureFin[0]);
                                $tabJourFermetureDebut = array_values($tabJourFermetureDebut);
                                $tabJourFermetureFin = array_values($tabJourFermetureFin);
                            }
                        }
                        $cptControl = 1;
                    } else if ($annee . "-" . $moisText . "-" . $jourText >= $tabJourVacanceDebut[0] && $annee . "-" . $moisText . "-" . $jourText <= $tabJourVacanceFin[0]) {
                        if ($class == "") {
                            $class = "class='caseJourFermeture'";
                        } 
                        if ($annee . "-" . $moisText . "-" . $jourText == $tabJourVacanceFin[0]) {
                            $nbrIndiceVacanceDebut = count($tabJourVacanceDebut);
                            if ($nbrIndiceVacanceDebut == 1) {
                                
                            } else {
                                unset($tabJourVacanceDebut[0]);
                                unset($tabJourVacanceFin[0]);
                                $tabJourVacanceDebut = array_values($tabJourVacanceDebut);
                                $tabJourVacanceFin = array_values($tabJourVacanceFin);
                            }
                        }
                        $cptControl = 1;
                    }

                    if ($annee . "-" . $moisText . "-" . $jourText == $date) {
                        if ($class != "class='caseWeek'") {
                            $class = "class='caseJourCourant'";
                        }
                    }
                    if ($annee . "-" . $moisText . "-" . $jourText <= $date && $cptControl == 0) {
                        echo"<tr $class><td><center>" . $jour . "</center></td><td><center>" . $valJour . "</center></td>"
                        . "<form action='employe.php' method='post'><td width='60px'>"
                        . "<input type='hidden' name='champSaisie' value=\"" . $moisText . "-" . $jourText . "\"/>"
                        . "<input type='submit' class='btnSaisie' name='saisie' value=\"" . $_SESSION[$moisText . "-" . $jourText] . "\"/></form></td></tr>";
                     
                    } else {
                        echo"<tr $class><td><center>" . $jour . "</center></td><td><center>" . $valJour . "</center></td>"
                        . "<td width='60px'></td></tr>";
                        $cptControl = 0;
                    }
                }
                echo"</table>";
                echo"</div>";
            }
        }
        ?>


    </body>
</html>
