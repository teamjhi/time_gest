<!DOCTYPE html>
<?php
session_start();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="./jquery-ui/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="./CSS/vacancesEmploye.css" media="all"/>
        <script>
            $(function() {

                $("#nouveauVacancesDebut").datepicker($.datepicker.regional["fr"]);
                $("#nouveauVacancesFin").datepicker($.datepicker.regional["fr"]);
            });

        </script>  
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
        $tabUser = $cmd->getAllUser();
        $cptFalse = 0;



        if (isset($_POST['ajouterVacances']) || isset($_POST['choixEmployeVacances']) || isset($_POST['supprimerVacances'])) {

            if (isset($_POST['choixEmployeVacances'])) {
                $_SESSION['user'] = $_POST['employe'];
            }

            if (isset($_POST['supprimerVacances'])) {
                $tabVacances = $cmd->deleteVacances($_POST['idVac']);
            }



            if (isset($_POST['ajouterVacances'])) {

                if (strlen($_POST['nouveauVacancesDebut']) != 10 || strlen($_POST['nouveauVacancesFin']) != 10) {
                    $cptFalse = 1;
                }
                if ($_POST['nouveauVacancesDebut'] == "" || $_POST['nouveauVacancesFin'] == "") {
                    $cptFalse = 1;
                }
                if ($_POST['nouveauVacancesDebut'] > $_POST['nouveauVacancesFin']) {
                    $cptFalse = 1;
                }
                if ($cptFalse == 0) {
                    $tabJourDebut = explode("/", $_POST['nouveauVacancesDebut']);
                    $jour_debut_vacances = $tabJourDebut[1];
                    $mois_debut_vacances = $tabJourDebut[0];
                    $annee_debut_vacances = $tabJourDebut[2];

                    $tabJourFin = explode("/", $_POST['nouveauVacancesFin']);
                    $jour_fin_vacances = $tabJourFin[1];
                    $mois_fin_vacances = $tabJourFin[0];
                    $annee_fin_vacances = $tabJourFin[2];

                    $dateDebut = $annee_debut_vacances . "-" . $mois_debut_vacances . "-" . $jour_debut_vacances;
                    $dateFin = $annee_fin_vacances . "-" . $mois_fin_vacances . "-" . $jour_fin_vacances;
                    $tabVacances = $cmd->addVacances($dateDebut, $dateFin, $_SESSION['user']);
                }
            }

            $tabVacances = $cmd->getVacances($_SESSION['user']);

            echo"<div id='content'>";
            echo"<div id='titre1'>VACANCES PLANIFIER</div>";
            echo"<div id='text1'>Début</div>";
            echo"<div id='text2'>Fin</div>";
            echo"<div id='vacances'>";
            echo"<table>";
            foreach ($tabVacances as $key => $value) {
                echo"<form action='vacancesEmploye.php' method='post' />";
                echo"<tr><td>" . $value['date_debut_vac'] . "</td><td width='80px'></td><td>" . $value['date_fin_vac'] . "</td>";
                echo"<input type='hidden' name='idVac' value=\"" . $value['id_vac'] . "\"/>";
                echo"<td width='80px'><td><input type='submit' name='supprimerVacances' value='X'/></td></tr>";
                echo"</form>";
            }
            echo"</table>";
            echo"</div>";


            echo"<div id='titre2'>AJOUTER DES VACANCES</div>";
            echo"<form action='vacancesEmploye.php' method='post' />";
            echo"<div id='text3'>Debut</div>";
            echo"<div id='champDebut'><input type='text'  id='nouveauVacancesDebut'  class='input' name='nouveauVacancesDebut' value=''/></div>";
            echo"<div id='text4'>Fin</div>";
            echo"<div id='champFin'><input type='text' id='nouveauVacancesFin'  class='input' name='nouveauVacancesFin' value=''/></div>";
            echo"<div id='btnAjouter'><input type='submit' name='ajouterVacances' class='btn' value='Ajouter'/></div>";
            echo"</form>";

            echo"<form action='admin.php' method='post' />";
            echo"<div id='btnRetour'><input type='submit' name='retour' class='btn' value='Retour'/></div>";
            echo"</form>";
            echo"</div>";
        } else {
            echo"<div id='content'>";
            echo"<form action='vacancesEmploye.php' method='post' />";
            echo"<div id='titre1'>CHOISIR L'EMPLOYÉ</div><div id='choixEmploye'><select name='employe'>";

            foreach ($tabUser as $key => $value) {
                $id = $value['id_emp'];
                $nom = $value['nom_emp'];
                $prenom = $value['prenom_emp'];

                echo"<option value=$id>$nom $prenom </option>";
            }


            echo "</select></div><div id='btnChoix'><input type='submit' class='btn' name='choixEmployeVacances' value='CHOISIR'/></div>";
            echo"</form>";
            echo"<form action='admin.php' method='post' />";
            echo"<div id='btnRetour1'><input type='submit' class='btn' name='retour' value='RETOUR'/></div>";
            echo"</form>";
            echo"</div>";
        }
        ?>
    </body>
</html>
