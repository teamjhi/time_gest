<?php

Class Mypdo EXTENDS PDO {

    // Constructeur
    function __construct() {


	try {
	    parent::__construct("mysql:hsot=localhost;dbname=ctm", "root", "");
	} catch (Exception $e) {
	    echo "Erreur : " . $e->getMessage() . "<br />";
	    echo "Erreur : " . $e->getCode() . "<br />";
	}
    }

    // Fonction : Cette fonction execute une requête 
    function query($query, $debug = 0) {

	if ($debug) {
	    echo "<br>";
	    echo $query;
	    echo "<br>";
	}

	try {
	    $result = parent::query($query);
	} catch (Exception $e) {
	    if ($debug) {
		echo "Erreur : " . $e->getMessage() . "<br />";
		echo "Erreur : " . $e->getCode() . "<br />";
	    }
	}
	return $result;
    }

}

?>