<?php

class Projet {

// Propriété
    protected $id;
    public $pdo;

// Constructeur
    function __construct($pdo) {
        $this->pdo = $pdo;
    }

    function test() {
        global $resultat;
    }

// Function qui va retourner toutes les informations sur un employé. L'employé sera choisit par son id
    function getUser($id) {
        $user = $this->pdo->query("Select * FROM t_employe WHERE id_emp='$id' ")->fetch(PDO::FETCH_ASSOC);
        return $user;
    }

    function getUserLogin($username) {
        $user = $this->pdo->query("Select id_emp FROM t_employe WHERE username_emp='$username' ")->fetch(PDO::FETCH_ASSOC);
        return $user;
    }

// Function qui va retourner toutes les informations sur les employée de l'entreprise.
    function getAllUser() {
        $user = $this->pdo->query("Select * FROM t_employe")->fetchAll(PDO::FETCH_ASSOC);
        return $user;
    }

// Function qui va ajouter un nouveau employé dans la base de donnée
    function addUser($nom, $prenom, $adresse, $npa, $localite, $telephone, $email, $age, $admin, $username, $password, $idPoste) {
        $sql = $this->pdo->query("INSERT INTO t_employe (id_emp,nom_emp,prenom_emp,adresse_emp,npa_emp,localite_emp,telephone_emp,email_emp,age_emp,admin_emp,username_emp,password_emp,id_pos) 
        VALUES('','$nom','$prenom','$adresse','$npa','$localite','$telephone','$email','$age','$admin','$username','$password','$idPoste')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va modifier les données d'un employé
    function updateUser($nom, $prenom, $adresse, $npa, $localite, $telephone, $email, $age, $admin, $username, $password, $poste, $idUser) {
        $sql = $this->pdo->query("UPDATE t_employe SET nom_emp='$nom', prenom_emp='$prenom', adresse_emp='$adresse',npa_emp='$npa',localite_emp='$localite',telephone_emp='$telephone',"
                . "email_emp='$email',age_emp='$age',admin_emp='$admin',username_emp='$username',password_emp='$password',id_pos='$poste' WHERE id_emp='$idUser'") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va retourner le nombre d'heure qu'un employé a 100% doit faire par jour dans l'entreprise
    function getTempsTravailJournalier() {
        $temps = $this->pdo->query("Select temps_travail_jour_ent FROM t_entreprise")->fetch(PDO::FETCH_ASSOC);
        return $temps;
    }

// Function qui va modifier  le nombre d'heure qu'un employé a 100% doit faire par jour dans l'entreprise
    function updateTempsTravailJournalier($temps) {
        $sql = $this->pdo->query("UPDATE t_entreprise SET temps_travail_jour_ent='$temps' ") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va retourner toutes les informations sur les jours fériées de l'entraprise
    function getJourFerie() {
        $jour = $this->pdo->query("Select *  FROM t_jourFerie ORDER BY date_jfe")->fetchAll(PDO::FETCH_ASSOC);
        return $jour;
    }

// Function qui va retourner l'id du jour férie par rapport a une date
    function getIdJourFerie($date) {
        $id = $this->pdo->query("Select id_jfe FROM t_jourFerie WHERE date_jfe='$date'")->fetch(PDO::FETCH_ASSOC);
        return $id;
    }

// Function qui va ajouter un jour férie dans la base de donnée
    function addJourFerie($date, $duree, $idEntreprise) {
        $sql = $this->pdo->query("INSERT INTO t_jourferie (id_jfe,date_jfe,duree_jfe,id_ent) 
        VALUES('','$date','$duree','$idEntreprise ')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va supprimer un jour férie dans la base de donnée
    function deleteJourFerie($id) {

        $sql = $this->pdo->query("DELETE FROM t_jourferie WHERE id_jfe=$id") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va retourner toutes les information sur les jours de ratttapage de l'entreprise
    function getJourRattrapage() {
        $jour = $this->pdo->query("Select *  FROM t_jourrattrapage ORDER BY date_jra  ")->fetchAll(PDO::FETCH_ASSOC);
        return $jour;
    }

// Function qui va ajouter un jour de rattrapage dans la base de donnée
    function addJourRattrapage($date, $idEntreprise) {
        $sql = $this->pdo->query("INSERT INTO t_jourrattrapage (id_jra,date_jra,id_ent) 
        VALUES('','$date','$idEntreprise ')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va supprimer un jour de rattrapage de la base de donnée
    function deleteJourRattrapage($id) {
        $sql = $this->pdo->query("DELETE FROM t_jourrattrapage WHERE id_jra='$id'");
    }

// Function qui va retourner toutes les informations sur les jours de fermeture de l'entreprise
    function getJourFermeture() {
        $jour = $this->pdo->query("Select *  FROM t_fermeture  ORDER BY date_debut_fer")->fetchAll(PDO::FETCH_ASSOC);
        return $jour;
    }

// Function qui va ajouter un jour de fermeture de l'entreprise
    function addJourFermeture($dateDebut, $dateFin, $idEntreprise) {
        $sql = $this->pdo->query("INSERT INTO t_fermeture (id_fer,date_debut_fer,date_fin_fer,id_ent) 
        VALUES('','$dateDebut','$dateFin','$idEntreprise ')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va supprimer un jour de fermeure de l'entreprise
    function deleteJourFermeture($id) {
        $sql = $this->pdo->query("DELETE FROM t_fermeture WHERE id_fer='$id'");
    }

// Function qui va les vacances d'un employé. 
    function getVacances($idUser) {
        $jour = $this->pdo->query("Select *  FROM t_vacances WHERE id_emp='$idUser' ORDER BY date_debut_vac")->fetchAll(PDO::FETCH_ASSOC);
        return $jour;
    }

// Function qui va ajouter des vacacnes a un employé
    function addVacances($dateDebut, $dateFin, $idUser) {
        $sql = $this->pdo->query("INSERT INTO t_vacances (id_vac,date_debut_vac,date_fin_vac,id_emp) 
        VALUES('','$dateDebut','$dateFin','$idUser ')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui supprimer les vacacnces d'un employé
    function deleteVacances($id) {
        $sql = $this->pdo->query("DELETE FROM t_vacances WHERE id_vac='$id'");
    }

// Function qui va retourner toutes les inforamtions sur les postes
    function getPoste() {
        $poste = $this->pdo->query("Select *  FROM t_poste")->fetchAll(PDO::FETCH_ASSOC);
        return $poste;
    }

    function getPosteNom($id) {
        $poste = $this->pdo->query("Select * FROM t_poste WHERE id_pos='$id'")->fetchAll(PDO::FETCH_ASSOC);
        return $poste;
    }

// Function qui va ajouter une periode de travail a un poste
    function addPeriode($dateDebut, $dateFin, $pourcentage, $nombreVacances, $idPoste) {
        $sql = $this->pdo->query("INSERT INTO t_periode (id_per,date_debut_per,date_fin_per,pourcentage_per,vacances_per,id_pos) 
        VALUES('','$dateDebut','$dateFin','$pourcentage','$nombreVacances','$idPoste')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

// Function qui va retourner toutes les periodes de travail d'un poste
    function getPeriode($idPoste) {
        $periode = $this->pdo->query("Select *  FROM t_periode WHERE id_pos='$idPoste' ORDER BY date_debut_per ")->fetchAll(PDO::FETCH_ASSOC);
        return $periode;
    }

// Function qui va retournerla periode de travail d'un poste en cours
    function getPeriodeUser($idPoste) {
        $periode = $this->pdo->query("Select *  FROM t_periode WHERE id_pos='$idPoste' ORDER BY date_debut_per DESC")->fetch(PDO::FETCH_ASSOC);
        return $periode;
    }

// Function qui va supprimer un eperiode de travail 
    function deletePeriode($id) {
        $sql = $this->pdo->query("DELETE FROM t_periode WHERE id_per='$id'");
    }

    function checkLogin($username, $password) {

        $tab = $this->pdo->query("SELECT username_emp,password_emp,admin_emp FROM t_employe WHERE username_emp='$username' AND password_emp='$password'")->fetch(PDO::FETCH_ASSOC);
        return $tab;
    }

    function addPlage($tempsDebut, $tempsFin, $temps, $date, $idUser) {
        $sql = $this->pdo->query("INSERT INTO t_plage (id_pla,temps_debut_pla,temps_fin_pla,total_temps_pla,date_pla,id_emp) 
        VALUES('','$tempsDebut','$tempsFin','$temps','$date','$idUser')") or die('Erreur SQL !' . $sql . '<br />' . mysql_error());
    }

    function getPlageUser($idUser) {
        $periode = $this->pdo->query("Select *  FROM t_plage WHERE id_emp='$idUser'  ORDER BY date_pla ")->fetchAll(PDO::FETCH_ASSOC);
        return $periode;
    }

    function getPlageUserDate($idUser, $date) {
        $periode = $this->pdo->query("Select *  FROM t_plage WHERE id_emp='$idUser' AND date_pla='$date' ORDER BY date_pla ")->fetchAll(PDO::FETCH_ASSOC);
        return $periode;
    }

    function getPlage($date) {
        $plage = $this->pdo->query("Select *  FROM t_plage WHERE date_pla='$date'")->fetchAll(PDO::FETCH_ASSOC);
        return $plage;
    }

    function deletePlage($id) {
        $sql = $this->pdo->query("DELETE FROM t_plage WHERE id_pla='$id'");
    }

}
?>

