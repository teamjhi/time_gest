<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/nouveauEmploye.css" media="all"/>
        <title></title>
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
        $cptFalse = 0;

        // Si on a cliquer sur créer un nouveau employé
        if (isset($_POST['nouveauEmp'])) {
	    
	    
	    /*Test des dnnées*/
	    
	    
            if ($_POST['nom'] == "" || $_POST['prenom'] == "" || $_POST['adresse'] == "" || $_POST['npa'] == "" || $_POST['localite'] == "" ||
                    $_POST['telephone'] == "" || $_POST['email'] == "" || $_POST['age'] == "" || $_POST['admin'] == "" || $_POST['username'] == "" || $_POST['password'] == "" || $_POST['poste'] == "") {
                $cptFalse = 1;
            }
            if (is_numeric($_POST['nom']) == TRUE || is_numeric($_POST['prenom']) == TRUE || is_numeric($_POST['localite']) == TRUE) {
                $cptFalse = 1;
            }

            if (strlen($_POST['npa']) != 4) {
                $cptFalse = 1;
            }
            if (strlen($_POST['telephone']) != 10) {
                $cptFalse = 1;
            }

            if ($cptFalse == 0) {
		
		/*Données correctes*/
                $insert = $cmd->addUser($_POST['nom'], $_POST['prenom'], $_POST['adresse'], $_POST['npa'], $_POST['localite'], $_POST['telephone'], $_POST['email'], $_POST['age'], $_POST['admin'], $_POST['username'], $_POST['password'], $_POST['poste']);
            }
        }
	// Formulaire
        echo"<div id='content'>";
       
        echo"<form action='nouveauEmploye.php' method='post'>";

        echo"<div id='titre1'>NOUVEAU EMPLOYÉ</div>";
        echo"<div id='formulaire'>";
        echo"<div id='text1'>NOM</div><div id='input1'><input type='text' class='input' name='nom' value=''/></div>";
        echo"<div id='text2'>PRENOM</div><div id='input2'><input type='text' class='input' name='prenom' value=''/></div>";
        echo"<div id='text3'>ADRESSE</div><div id='input3'><input type='text' class='input' name='adresse' value=''/></div>";
        echo"<div id='text4'>NPA</div><div id='input4'><input type='text' class='input' name='npa' value=''/></div>";
        echo"<div id='text5'>LOCALITE</div><div id='input5'><input type='text' class='input' name='localite' value=''/></div>";
        echo"<div id='text6'>TELEPHONE</div><div id='input6'><input type='text'class='input' name='telephone' value=''/></div>";
        echo"<div id='text7'>MAIL</div><div id='input7'><input type='text' class='input' name='email' value=''/></div>";
        echo"<div id='text8'>AGE</div><div id='input8'><input type='text' name='age' class='input' value=''/></div>";
        echo"<div id='text9'>ADMIN</div><div id='input9'><select name='admin'><option value='0'>0</option><option value='1'>1</option></select></div>";
        echo"<div id='text10'>NOM D'UTILISATEUR</div><div id='input10'><input type='text' class='input' name='username' value=''/></div>";
        echo"<div id='text11'>MOT DE PASSE</div><div id='input11'><input type='password' class='input' name='password' value=''/></div>";

        echo"<div id='text12'>POSTE</div><div id='input12'><select name='poste'>";

        $tabPoste = $cmd->getPoste();

        // Boucle qui va parcourir le tableau
        foreach ($tabPoste as $key => $value) {
            $id = $value['id_pos'];
            $nom = $value['nom_pos'];


            echo"<option value=$id>$nom</option>";
        }
        echo "</select></div>";

        echo"<div id='btnAjouter'><input type='submit' class='btn' name='nouveauEmp' value='CRÉER'/></div>";
        echo"</form>";


        echo"<form action='admin.php' method='post' />";
        echo"<div id='btnRetour'><input type='submit' class='btn' name='retour' value='RETOUR'/></div>";
        echo"</form>";

      
        echo"</div>";
        ?>
    </body>
</html>
