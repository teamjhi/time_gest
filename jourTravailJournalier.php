<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/travailJournalier.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="./CSS/admin.css" media="all"/>
	

        <title></title>
    </head>
    <body>
	<?php
	require("./../config/time_gest/config.cfg.php");
	require ("./Class/inc.class.php");
	require ("./Class/function.class.php");
	$pdo = new Mypdo();
	$cmd = new Projet($pdo);
	$cptFalse = 0;


	// Si on a cliquer sur le bouton pour modifier le temps de travail
	if (isset($_POST['tempsTravailmodif'])) {

            
            /*Test de vérification des données*/
            
	    if (is_numeric($_POST['heure']) == FALSE || is_numeric($_POST['min']) == FALSE)
		$cptFalse++;
	    if ($_POST['heure'] == "") {
		$cptFalse++;
	    }
	    if ($_POST['min'] > 60 || $_POST['heure'] > 24) {
		$cptFalse++;
	    }
	    if (strlen($_POST['heure']) > 2 || strlen($_POST['min']) > 2) {
		$cptFalse++;
	    }
	    if (strlen($_POST['min']) == 1) {
		$_POST['min'] = "0" . $_POST['min'];
	    }
	    if ($_POST['min'] == "") {
		$_POST['min'] = "00";
	    }
	    if (strlen($_POST['heure']) == 2) {

		$tabPoint[0] = substr($_POST['heure'], 0, 1);
		$tabPoint[1] = substr($_POST['heure'], 1, 1);
		if ($tabPoint[0] == "." || $tabPoint[1] == ".") {
		    $cptFalse++;
		}
	    }
	    if (strlen($_POST['min']) == 2) {

		$tabPoint[0] = substr($_POST['min'], 0, 1);
		$tabPoint[1] = substr($_POST['min'], 1, 1);
		if ($tabPoint[0] == "." || $tabPoint[1] == ".") {
		    $cptFalse++;
		}
	    }

	    if (strlen($_POST['heure']) == 1) {
		$_POST['heure'] = "0" . $_POST['heure'];
	    }
	    if ($cptFalse == 0) {
		$temps = $_POST['heure'] . $_POST['min'];
		// Modification dans la base de données
		$update = $cmd->updateTempsTravailJournalier($temps);
	    }
	}


	
	$tabTemps = $cmd->getTempsTravailJournalier();
	
        // Variable qui contient le temps de travail journalier de l'entreprise
	$temps = $tabTemps['temps_travail_jour_ent'];
        //Substr : Va séparer la valeur 
	$heure = substr($temps, 0, 2);
	$min = substr($temps, 2, 4);

	echo"<div id='content'>";
	echo"<form action='jourTravailJournalier.php' method='post' />";
	echo"<div id='titre'>TEMPS À EFFECTUER PAR JOUR</div>"
	. "<div id='text1'>HEURES</div> <div id='champHeure'><input type='text' class='input'  name='heure' value=\"" . $heure . "\"/></div>"
	. "<div id='text2'>MINUTES</div> <div id='champMinute'><input type='text' class='input'  name='min' value=\"" . $min . "\"/></div>";
	echo"<div id='btnModif'><input type='submit' name='tempsTravailmodif' class='btn' value='MODIFIER'/></div>";
	echo"</form>";
	echo"<form action='admin.php' method='post' />";
	echo"<div id='btnRetour'><input type='submit' name='retour' class='btn' value='RETOUR'/></div>";
	echo"</form>";
	echo"</div>";
	?>
    </body>
</html>
