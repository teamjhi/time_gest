<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
	<script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="./jquery-ui/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="./jourFermeture.css" media="all"/>
 <!--Script qui appeller le calendrier-->
	<script>
	   
	    $(function() {

		$("#nouveauJourFermetureDebut").datepicker($.datepicker.regional["fr"]);
		$("#nouveauJourFermetureFin").datepicker($.datepicker.regional["fr"]);
	    });

	</script>  
    </head>
    <body>
	<?php
	require("./../config/time_gest/config.cfg.php");
	require ("./Class/inc.class.php");
	require ("./Class/function.class.php");
	$pdo = new Mypdo();
	$cmd = new Projet($pdo);
	$cptFalse = 0;


	// Si on a cliquer sur le bouton pour ajouter un jour de fermeture
	if (isset($_POST['ajouterJourFermeture'])) {

	     /*Test des données*/
	    
	    if (strlen($_POST['nouveauJourFermetureDebut']) != 10 || strlen($_POST['nouveauJourFermetureFin']) != 10) {
		$cptFalse = 1;
	    }
	    if ($_POST['nouveauJourFermetureDebut'] == "" || $_POST['nouveauJourFermetureFin'] == "") {
		$cptFalse = 1;
	    }
	    if ($_POST['nouveauJourFermetureDebut'] > $_POST['nouveauJourFermetureFin']) {
		$cptFalse = 1;
	    }
	    if ($cptFalse == 0) {
		$tabJourDebut = explode("/", $_POST['nouveauJourFermetureDebut']);
		$jour_debut_fermeture = $tabJourDebut[1];
		$mois_debut_fermeture = $tabJourDebut[0];
		$annee_debut_fermeture = $tabJourDebut[2];

		$tabJourFin = explode("/", $_POST['nouveauJourFermetureFin']);
		$jour_fin_fermeture = $tabJourFin[1];
		$mois_fin_fermeture = $tabJourFin[0];
		$annee_fin_fermeture = $tabJourFin[2];

		$dateDebut = $annee_debut_fermeture . "-" . $mois_debut_fermeture . "-" . $jour_debut_fermeture;
		$dateFin = $annee_fin_fermeture . "-" . $mois_fin_fermeture . "-" . $jour_fin_fermeture;

		$insert = $cmd->addJourFermeture($dateDebut, $dateFin, 1);
		// Si on a cliquer sur le bouton pour supprimer un jour de fermeture
	    }
	} else if (isset($_POST['supprimerJourFermeture'])) {
	    $delete = $cmd->deleteJourFermeture($_POST['idJourFermeture']);
	}
	// Tableau qui va contenir le resultat de la function
	$tabJourFermeture = $cmd->getJourFermeture();

	if ($cptFalse > 0) {
	    $style = "background_color_false";
	} else {
	    $style = "";
	    $cptFalse = 0;
	}

	echo"<div id='content'>";
	echo"<div id='titre1'>LES JOURS DE FERMETURES DE L'ENTREPRISE</div>";
	// Boucle qui va parcourir le tableau
	foreach ($tabJourFermeture as $key => $value) {
	    // Variable qui va contenir la date de debut de fermeture
	    $jourFermetureDebut[$key] = $value['date_debut_fer'];
	    // Variable qui va contenir la date de fin de fermeture
	    $jourFermetureFin[$key] = $value['date_fin_fer'];
	    // Variable qui va contenir l'id fermeture
	    $id[$key] = $value['id_fer'];
	    echo"<div id='jourFermeture'><form action='jourFermeture.php' method='post' />";
	    echo"<input type='text' name='jourDebutFermeture' value=\"" . $jourFermetureDebut[$key] . "\"/>";
	    echo"<input type='text' name='jourFinFermeture' value=\"" . $jourFermetureFin[$key] . "\"/>";
	    echo"<input type='hidden' name='idJourFermeture' value=\"" . $id[$key] . "\"/>";
	    echo"<input type='submit' name='supprimerJourFermeture' value='X'/><br/>";
	    echo"</form></div>";
	}


	echo"<div id='titre2'>AJOUTER DES JOURS DE FERMETURES</div>";
	echo"<form action='jourFermeture.php' method='post' />";
	echo"<div id='text1'>Debut</div><div id='jourNouveauDebut'><input type='text' id='nouveauJourFermetureDebut' name='nouveauJourFermetureDebut' class='input'  value=''/></div>";
	echo"<div id='text2'>Fin</div><div id='jourNouveauFin'><input type='text' id='nouveauJourFermetureFin' name='nouveauJourFermetureFin' class='input' value=''/></div>";
	echo"<div id='btnAjouter'><input type='submit' class='btn' name='ajouterJourFermeture' value='Ajouter'/></div>";
	echo"</form>";

	echo"<form action='admin.php' method='post' />";
	echo"<div id='btnRetour'><input type='submit' class='btn' name='retour' value='Retour'/></div>";
	echo"</form>";
	echo"</div>";
	?>
    </body>
</html>
