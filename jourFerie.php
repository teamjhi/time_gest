<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

        <script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="./jquery-ui/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="./CSS/admin.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="./CSS/jourferie.css" media="all"/>
	<!--Script qui appeller le calendrier-->
        <script>

	    $(function() {

		$("#nouveauJourFerie").datepicker($.datepicker.regional["fr"]);
	    });

        </script>   
    </head>
    <body>
	<?php
	require("./../config/time_gest/config.cfg.php");
	require ("./Class/inc.class.php");
	require ("./Class/function.class.php");
	$pdo = new Mypdo();
	$cmd = new Projet($pdo);
	$cptFalse = 0;


	// Si on a cliquer sur le bouton pour ajouter un jour ferie
	if (isset($_POST['ajouterJourFerie'])) {

	    /* Test des données */

	    if (strlen($_POST['nouveauJourFerie']) != 10) {
		$cptFalse = 1;
	    }
	    if ($_POST['nouveauJourFerie'] == "") {
		$cptFalse = 1;
	    }
	    if ($cptFalse == 0) {

		/* Données correctes */
		$tabJourDebut = explode("/", $_POST['nouveauJourFerie']);
		$jour_ferie = $tabJourDebut[1];
		$mois_ferie = $tabJourDebut[0];
		$annee_ferie = $tabJourDebut[2];

		$date = $annee_ferie . "-" . $mois_ferie . "-" . $jour_ferie;



		if ($_POST['duree'] == 0) {
		    $duree = 0;
		} else {
		    $duree = 1;
		}
		$insert = $cmd->addJourFerie($date, $duree, 1);
	    }
	    // Si on a cliquer sur le bouton pour supprimer un jour ferie   
	} else if (isset($_POST['supprimerJourFerie'])) {
	    $delete = $cmd->deleteJourFerie($_POST['idJourFerie']);
	}
	// Tableau qui va contenir le resultat de la function
	$tabJourFerie = $cmd->getJourFerie();

	if ($cptFalse > 0) {
	    $style = "background_color_false";
	} else {
	    $style = "";
	    $cptFalse = 0;
	}
	echo"<div id='content'>";
	echo"<div id='titre1'>LES JOURS FÉRIÉS DE L'ENTREPRISE</div>";
	// Boucle qui va parcourir le tableau
	echo"<div id='jourFerieEnt'>";
	foreach ($tabJourFerie as $key => $value) {
	    // Variable qui va contenir l'id des jours ferie
	    $id[$key] = $value['id_jfe'];
	    // Variable qui va contenir la date du jour ferie
	    $jour[$key] = $value['date_jfe'];
	    echo"<form action='jourFerie.php' method='post' />";
	    echo"<input type='text' name='jourFerie' value=\"" . $jour[$key] . "\"/>";
	    echo"<input type='hidden' name='idJourFerie' value=\"" . $id[$key] . "\"/>";
	    echo"<input type='submit' name='supprimerJourFerie' value='X'/><br/>";
	    echo"</form>";
	}
	echo"</div>";

	echo"<div id='titre2'> AJOUTER UN JOUR FÉRIE </div>";
	echo"<form action='jourFerie.php' method='post' />";
	echo"<div id='jourNouveau'><input  id='nouveauJourFerie'  type='text' name='nouveauJourFerie' value=''/></div>";
	echo"<div id='jourDuree'>Durée du jour  <select name='duree'>";
	// Boucle qui va parcourir le tableau


	echo"<option value=1>Entier</option>";
	echo"<option value=0>Demi</option>";

	echo "</select></div>";


	echo"<div id='btnAjouter'><input type='submit'class='btn' name='ajouterJourFerie' value='Ajouter'/></div>";
	echo"</form>";

	echo"<form action='admin.php' method='post' />";
	echo"<div id='btnRetour'><input type='submit' class='btn' name='retour' value='Retour'/></div>";
	echo"</form>";
	echo"</div>";
	?>
    </body>
</html>
