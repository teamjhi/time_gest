<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
	<script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="./jquery-ui/jquery-ui.min.css"/>
	<link rel="stylesheet" type="text/css" href="./CSS/admin.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="./CSS/jourRattrapage.css" media="all"/>
	<script>
	    $(function() {

		$("#nouveauJourRattrapage").datepicker($.datepicker.regional["fr"]);
	    });

	</script>  
    </head>
    <body>
	<?php
	require("./../config/time_gest/config.cfg.php");
	require ("./Class/inc.class.php");
	require ("./Class/function.class.php");
	$pdo = new Mypdo();
	$cmd = new Projet($pdo);
	$cptFalse = 0;


	// Si on a cliquer sur le bouton pour ajouter un jour de rattrapage
	if (isset($_POST['ajouterJourRattrapage'])) {

	    /* Test des données */
	    if (strlen($_POST['nouveauJourRattrapage']) != 10) {
		$cptFalse = 1;
	    }
	    if ($_POST['nouveauJourRattrapage'] == "") {
		$cptFalse = 1;
	    }
	    if ($cptFalse == 0) {

		$tabJourDebut = explode("/", $_POST['nouveauJourRattrapage']);
		$jour_ferie = $tabJourDebut[1];
		$mois_ferie = $tabJourDebut[0];
		$annee_ferie = $tabJourDebut[2];

		$date = $annee_ferie . "-" . $mois_ferie . "-" . $jour_ferie;

		$insert = $cmd->addJourRattrapage($date, 1);
		// Si on a cliquer sur le bouton pour supprimer un jour rattrapage
	    }
	} else if (isset($_POST['supprimerJourRattrapage'])) {
	    $delete = $cmd->deleteJourRattrapage($_POST['idJourRattrapage']);
	}
	// Tableau qui va contenir le resultat de la fonction
	$tabJourRattrapage = $cmd->getJourRattrapage();

	if ($cptFalse > 0) {
	    $style = "background_color_false";
	} else {
	    $style = "";
	    $cptFalse = 0;
	}
	echo"<div id='content'>";
	echo"<div id='titre1'>LES JOURS DE RATTRAPAGES DE L'ENTREPRISE</div>";
	// BOucle qui va parcourir le tableau
	echo"<div id='jourRattrapagesEnt'>";
	foreach ($tabJourRattrapage as $key => $value) {
	    // Variable qui va contenir la date
	    $jourRattrapage[$key] = $value['date_jra'];
	    // Variable qui va contenir l'id
	    $id[$key] = $value['id_jra'];
	    echo"<form action='jourRattrapage.php' method='post' />";
	    echo"<input type='text' name='jourRattrapage' value=\"" . $jourRattrapage[$key] . "\"/>";
	    echo"<input type='hidden' name='idJourRattrapage' value=\"" . $id[$key] . "\"/>";
	    echo"<input type='submit' name='supprimerJourRattrapage' value='X'/><br/>";
	    echo"</form>";
	}
	echo"</div>";

	echo"<div id='titre2'>AJOUTER DES JOURS DE RATTRAPAGES</div>";
	echo"<form action='jourRattrapage.php' method='post' />";
	echo"<div id='jourNouveau'><input type='text' id='nouveauJourRattrapage'  name='nouveauJourRattrapage' value=''/></div>";
	echo"<div id='btnAjouter'><input type='submit' name='ajouterJourRattrapage'  class='btn' value='Ajouter'/></div>";
	echo"</form>";

	echo"<form action='admin.php' method='post' />";
	echo"<div id='btnRetour'><input type='submit' name='retour'  class='btn' value='Retour'/></div>";
	echo"</form>";
	echo"</div>";
	?>
    </body>
</html>
