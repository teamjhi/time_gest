<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/login.css" media="all"/>
        <title></title>
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);


        //Initialisation des variables
        $cptFalse = 1;
        $texteFalse = "";


        //La condition sera remplit si l'utilisatuer a cliquer sur le bouton Login
        if (isset($_POST['login'])) {

            // Récupération des données
            $username = $_POST['username'];
            $password = $_POST['password'];


            //Vérification des données
            $tab = $cmd->checkLogin($username, $password);

            //Variables administrateur
            $admin = $tab['admin_emp'];


            // Si les données sont incorrectes
            if ($tab == "") {
                $texteFalse = "Le nom d'utilisater ou le mot de passe est incorrect";
                $cptFalse = 1;
                // Si l'utilisateur est un administrateur
            } else if ($admin == 1) {

                echo"<div id='loginCheck'>";
                $_SESSION['username'] = $tab['username_emp'];
                $_SESSION['password'] = $tab['password_emp'];
                $userId = $cmd->getUserLogin($_SESSION['username']);
                $user = $cmd->getUser($userId['id_emp']);

                echo "<form action='admin.php' method='post' >";
                echo "<div id='textLogin1'>Bonjour " . $user['nom_emp'] . "</div>";
                echo "<div id='text3'>INTERFACE ADMINISTRATEUR</div>";
                echo "<div id='adminInterface'><input type='submit' class='btnGrand 'value='OUVRIR' name='login'/></div>";
                echo "</form>";
                $cptFalse = 0;
                echo "</div>";
            } else {
                echo"<div id='loginCheck'>";
                $_SESSION['username'] = $tab['username_emp'];
                $_SESSION['password'] = $tab['password_emp'];
                //Récuperation des données de l'utisateur
                $userId = $cmd->getUserLogin($_SESSION['username']);
                $user = $cmd->getUser($userId['id_emp']);
                echo "<form action='employe.php' method='post' >";
                echo "<div id='textLogin2'>Bonjour " . $user['nom_emp'] . "</div>";
                echo "<div id='text4'>CALENDRIER</div>";
                echo "<div id='calendrier'><input type='submit' class='btnGrand' value='OUVRIR' name='login'/></div>";
                echo "</form>";
                echo "</div>";
                $cptFalse = 0;
            }
        }
        if ($cptFalse == 1) {

            echo"<div id='login'>";
            echo"<div id='titre'>CONNEXION</div>";
            echo"<form action='login.php' method='post' >";
            echo "<div id='falseLogin'>" . $texteFalse . "</div>";
            echo "<div id='text1'>Nom d'utilisateur </div><div id='username'><input type='text' class='input' name='username'/></div>";
            echo "<div id='text2'>Mot de passe </div><div id='password'> <input type='password' class='input' name='password'/></div>";
            echo "<div id='btnLogin'><input type='submit' class='btn' value='LOGIN' name='login'/></div>";
            echo "</form>";
            echo"</div>";
        }
        ?>
    </body>
</html>
