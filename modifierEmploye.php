<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/modifierEmploye.css" media="all"/>
        <title></title>
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
        $cptFalse = 0;

        // Si on a cliquer sur modifié un  employé
        if (isset($_POST['modifierEmp'])) {

	    /*Test les donéées récuperer*/
            if ($_POST['nom'] == "" || $_POST['prenom'] == "" || $_POST['adresse'] == "" || $_POST['npa'] == "" || $_POST['localite'] == "" ||
                    $_POST['telephone'] == "" || $_POST['email'] == "" || $_POST['age'] == "" || $_POST['admin'] == "" || $_POST['username'] == "" || $_POST['password'] == "" || $_POST['poste'] == "") {
                $cptFalse = 1;
            }
            if (is_numeric($_POST['nom']) == TRUE || is_numeric($_POST['prenom']) == TRUE || is_numeric($_POST['localite']) == TRUE) {
                $cptFalse = 1;
            }

            if (strlen($_POST['npa']) == 4) {
                $cptFalse = 1;
            }
            if (strlen($_POST['telephone']) == 10) {
                $cptFalse = 1;
            }
	    if($_POST['admin']!=1 || $_POST['admin']!=0){
		$cptFalse = 1;
	    }
            if ($cptFalse == 0) {
                if ($_POST['poste'] == "vide") {
                    $_POST['poste'] = $_POST['idPoste'];
                }
		// Modification des données dans la base
                $userUpdate = $cmd->updateUser($_POST['nom'], $_POST['prenom'], $_POST['adresse'], $_POST['npa'], $_POST['localite'], $_POST['telephone'], $_POST['email'], $_POST['age'], $_POST['admin'], $_POST['username'], $_POST['password'], $_POST['poste'], $_POST['idUser']);
            }
        }
        // Si on a choisir l'employé
        if (isset($_POST['choixEmploye'])) {
            //Tableau qui va contenir toutes le information de l'employé choisit.
            $user = $cmd->getUser($_POST['employe']);


            echo"<div id='content'>";

            echo"<form action='modifierEmploye.php' method='post'>";

            echo"<div id='titre2'>EMPLOYÉ</div>";
            echo"<div id='formulaire'>";
            echo"<div id='text1'>NOM</div><div id='input1'><input type='text' class='input' name='nom'value=\"" . $user['nom_emp'] . "\"/></div>";
            echo"<div id='text2'>PRENOM</div><div id='input2'><input type='text' class='input' name='prenom' value=\"" . $user['prenom_emp'] . "\"/></div>";
            echo"<div id='text3'>ADRESSE</div><div id='input3'><input type='text' class='input' name='adresse' value=\"" . $user['adresse_emp'] . "\"/></div>";
            echo"<div id='text4'>NPA</div><div id='input4'><input type='text' class='input' name='npa' value=\"" . $user['npa_emp'] . "\"/></div>";
            echo"<div id='text5'>LOCALITE</div><div id='input5'><input type='text' class='input' name='localite' value=\"" . $user['localite_emp'] . "\"/></div>";
            echo"<div id='text6'>TELEPHONE</div><div id='input6'><input type='text'class='input' name='telephone' value=\"" . $user['telephone_emp'] . "\"/></div>";
            echo"<div id='text7'>MAIL</div><div id='input7'><input type='text' class='input' name='email' value=\"" . $user['email_emp'] . "\"/></div>";
            echo"<div id='text8'>AGE</div><div id='input8'><input type='text' name='age' class='input' value=\"" . $user['age_emp'] . "\"/></div>";
            echo"<div id='text9'>ADMIN</div><div id='input9'><input type='text' name='admin' class='input' value=\"" . $user['admin_emp'] . "\"/></div>";
            echo"<div id='text10'>NOM D'UTILISATEUR</div><div id='input10'><input type='text' class='input' name='username'value=\"" . $user['username_emp'] . "\"/></div>";
            echo"<div id='text11'>MOT DE PASSE</div><div id='input11'><input type='password' class='input' name='password' value=\"" . $user['password_emp'] . "\"/></div>";


            echo"<div id='text12'>POSTE</div><div id='input12'><select name='poste'>";

            $tabPoste = $cmd->getPoste();

            echo"<option value='vide'></option>";
            // Boucle qui va parcourir le tableau
            foreach ($tabPoste as $key => $value) {
                $id = $value['id_pos'];
                $nom = $value['nom_pos'];


                echo"<option value=$id>$nom</option>";
            }
            echo "</select></div>";
            echo"<div id='btnAjouter'><input type='submit' class='btn' name='modifierEmp' value='MODIFIER'/></div>";
            echo"<input type='hidden' name='idPoste' value=\"" . $user['id_pos'] . "\">";
            echo"<input type='hidden' name='idUser' value=\"" . $user['id_emp'] . "\">";

            echo"</form>";


            echo"<form action='admin.php' method='post' />";
            echo"<div id='btnRetour'><input type='submit' class='btn' name='retour' value='Retour'/></div>";
            echo"</form>";


            echo"</div>";
        } else {
            echo"<div id='content'>";
            // tableau qui va contenir tous les employes de l'entreprises
            $tabUser = $cmd->getAllUser();

            echo"<form action='modifierEmploye.php' method='post' />";
            echo"<div id='titre1'>CHOISIR L'EMPLOYÉ</div><div id='choixEmploye'><select name='employe'>";
            // Boucle qui va parcourir le tableau
            foreach ($tabUser as $key => $value) {
                $id = $value['id_emp'];
                $nom = $value['nom_emp'];
                $prenom = $value['prenom_emp'];

                echo"<option value=$id>$nom $prenom </option>";
            }


            echo "</select></div><div id='btnChoix'><input type='submit' name='choixEmploye' class='btn' value='CHOISIR'/></div>";
            echo"</form>";
            echo"<form action='admin.php' method='post' />";
            echo"<dic id ='btnRetour1'><input type='submit' class='btn' name='retour' value='RETOUR'/></div>";
            echo"</form>";

            echo"</div>";
        }
        ?>
    </body>
</html>
