<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="./jquery-ui/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="./CSS/periodeEmploye.css" media="all"/>
	<!--Script qui appeller le calendrier-->
        <script>
            $(function() {

                $("#dateDebut").datepicker($.datepicker.regional["fr"]);
                $("#dateFin").datepicker($.datepicker.regional["fr"]);
            });

        </script>  
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
        $cptFalse = 0;

        $tabUser = $cmd->getAllUser();


        // Si on clique sur choisir un employer ou ajouter une nouvelle periode ou supprimer une periode on va rentrer dans la conditions. 
        if (isset($_POST['choixEmployePeriode']) || isset($_POST['ajouterPeriode']) || isset($_POST['supprimerPeriode'])) {

            // Si on clique sur choisir un employé
            if (isset($_POST['choixEmployePeriode'])) {
                $_SESSION['user'] = $_POST['employe'];
                $poste = $cmd->getUser($_SESSION['user']);
                $_SESSION['poste'] = $poste['id_pos'];
            }


            // Si on clique sur supprimer un periode
            if (isset($_POST['supprimerPeriode'])) {
                $deletePoste = $cmd->deletePeriode($_POST['idPeriode']);
            }
            // Si on clique sur ajouter une periode
            if (isset($_POST['ajouterPeriode'])) {

                if (strlen($_POST['dateDebut']) != 10 || strlen($_POST['dateFin']) != 10) {
                    $cptFalse = 1;
                }
                if ($_POST['dateDebut'] == "" || $_POST['dateFin'] == "") {
                    $cptFalse = 1;
                }
                if ($_POST['dateDebut'] > $_POST['dateFin']) {
                    $cptFalse = 1;
                }
                if (is_numeric($_POST['pourcentage']) == FALSE || is_numeric($_POST['nombreVacacnes']) == FALSE) {
                    $cptFalse = 1;
                }

                if ($cptFalse == 0) {
                    $tabJourDebut = explode("/", $_POST['dateDebut']);
                    $jour_debut_periode = $tabJourDebut[1];
                    $mois_debut_periode = $tabJourDebut[0];
                    $annee_debut_periode = $tabJourDebut[2];

                    $tabJourFin = explode("/", $_POST['dateFin']);
                    $jour_fin_periode = $tabJourFin[1];
                    $mois_fin_periode = $tabJourFin[0];
                    $annee_fin_periode = $tabJourFin[2];

                    $dateDebut = $annee_debut_periode . "-" . $mois_debut_periode . "-" . $jour_debut_periode;
                    $dateFin = $annee_fin_periode . "-" . $mois_fin_periode . "-" . $jour_fin_periode;


                    $addPeriode = $cmd->addPeriode($dateDebut, $dateFin, $_POST['pourcentage'], $_POST['nombreVacances'], $_SESSION['poste']);
                }
            }
            // Tableau qui va contenir les periode de l'employé sélectionné
            $tabPeriode = $cmd->getPeriode($_SESSION['poste']);


            echo"<div id='content'>";
            echo"<div id='titre1'>PÉRIODE EN COURS</div>";
            echo"<div id='text1'>DÉBUT</div>";
            echo"<div id='text2'>FIN</div>";
            echo"<div id='text3'>POURCENTAGE</div>";
            echo"<div id='text4'>VACANCES</div>";

            // Boucle qui va parcourir le tableau
            echo"<div id='periode'>";
            echo"<table>";
            foreach ($tabPeriode as $key => $value) {
                
                echo"<form action='periodeEmploye.php' method='post' />";
                echo"<tr><td>" . $value['date_debut_per'] . "</td><td width='10px'></td><td>" . $value['date_fin_per'] . "</td><td width='50px'><td>" . $value['pourcentage_per'] . "</td><td width='85px'><td>" . $value['vacances_per'] . "</td><td width='50px'></td>";
                echo"<input type='hidden' name='idPeriode' value=\"" . $value['id_per'] . "\"/>";
                echo"<td><input type='submit' name='supprimerPeriode' value='X'/></td></tr>";
                echo"</form>";
            }
            echo"</table>";
            echo"</div>";

            echo"<div id='titre2'>NOUVELLE PÉRIODE</div>";
            echo"<div id='text5'>DÉBUT</div>";
            echo"<div id='text6'>FIN</div>";
            echo"<form action='periodeEmploye.php' method='post' />";
            echo"<div id='champDebut'><input type='text' id='dateDebut' class='input' name='dateDebut' value=''/></div>";
            echo"<div id='champFin'><input type='text' id='dateFin' class='input' name='dateFin' value=''/></div>";
            echo"<div id='text7'>POURCENTAGE</div>";
            echo"<div id='champPourcentage'><input type='text'  class='input' name='pourcentage' value=''/></div>";
            echo"<div id='text8'>JOURS DE VACANCES</div>";
            echo"<div id='champVacances'><input type='text'  class='input' name='nombreVacances' value=''/></div>";
            echo"<div id='btnAjouter'><input type='submit' class='btn' name='ajouterPeriode' value='AJOUTER'/></div>";
            echo"</form>";
            echo"<form action='admin.php' method='post' />";
            echo"<div id='btnRetour'><input type='submit' name='retour' class='btn' value='RETOUR'/></div>";
            echo"</form>";

            echo"</div>";
        } else {
            echo"<div id='content'>";
            echo"<form action='periodeEmploye.php' method='post' />";
            echo"<div id='titre1'>CHOISIR L'EMPLOYÉ</div><div id='choixEmploye'><select name='employe'>";

            foreach ($tabUser as $key => $value) {
                $id = $value['id_emp'];
                $nom = $value['nom_emp'];
                $prenom = $value['prenom_emp'];

                echo"<option value=$id>$nom $prenom </option>";
            }


            echo "</select></div><div id='btnChoix'><input type='submit' class='btn' name='choixEmployePeriode' value='CHOISIR'/></div>";
            echo"</form>";
            echo"<form action='admin.php' method='post' />";
            echo"<div id='btnRetour1'><input type='submit' class='btn' name='retour' value='RETOUR'/></div>";
            echo"</form>";
            echo"</div>";
        }
        ?>
    </body>
</html>
