<!DOCTYPE html>
<?php
session_start();

for ($i = 1; $i <= 12; $i++) {

    if (strlen($i) == 1) {
	$i = "0" . $i;
    }
    $_SESSION[$i . "_total"] = "";
}

function formatHeureTotalJour($temps, $temps2) {

    $heure = $temps;
    $min = $temps2;

    if ($min > 60) {

	$valMin = $min / 60;
	$tab = explode(".", $valMin);
	$heure+=$tab[0];
	$min = 60 / (100 / ($tab[1] * 10));
    }


    $min = substr($min, 0, 2);
    return $heure . ":" . $min;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="./jquery-ui/external/jquery/jquery.js"></script>
        <script src="./jquery-ui/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./CSS/impression.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="./CSS/print.css" media="print"/>
        <title></title>
    </head>
    <body>
	<?php
	require("./../config/time_gest/config.cfg.php");
	require ("./Class/inc.class.php");
	require ("./Class/function.class.php");
	$pdo = new Mypdo();
	$cmd = new Projet($pdo);
	$annee = 2015;
	$tab_mois_nom = array(1 => "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre", "Annuel");

	if (isset($_POST['impressionAdmin'])) {

	    // tableau qui va contenir tous les employes de l'entreprises
	    echo"<div id='content'>";
	    echo"<div id='titre1'>CHOIX DE L'IMPRESSION</div>";

	    echo"<form action = 'impression.php' method = 'post' />";
	    $tabUser = $cmd->getAllUser();
	    echo"<table>";
	    echo"<form action='impression.php' method='post' />";
	    echo"<div id='text2'>CHOISIR L'EMPLOYÉ</div>";
	    echo"<div id='employe'><select name='employe'>";
	    // Boucle qui va parcourir le tableau
	    foreach ($tabUser as $key => $value) {
		$id = $value['id_emp'];
		$nom = $value['nom_emp'];
		$prenom = $value['prenom_emp'];

		echo"<option value=$id>$nom $prenom </option>";
	    }


	    echo "</select></div>";
	    echo"<div id='text3'>CHOISIR LE MOIS</div>";

	    echo"<div id='mois1'><select name='mois'>";
	    // Boucle qui va parcourir le tableau

	    for ($i = 1; $i <= 13; $i++) {
		echo"<option value=$i>$tab_mois_nom[$i]</option>";
	    }
	    echo "</select></div>";

	    echo"<div id='btnApercu2'><input type = 'submit' class='btn' name = 'apercuAdmin' value = 'VALIDER'/></div>";
	    echo"</form>";

	    echo"<form action = 'admin.php' method = 'post' />";
	    echo"<div id='btnRetour2'><input type = 'submit' name = 'retour' class='btn' value = 'RETOUR'/></div>";
	    echo"</form>";
	    echo"<div>";
	} else {
	    if (isset($_POST['apercu']) || isset($_POST['apercuAdmin'])) {

		if (isset($_POST['apercuAdmin'])) {
		    $user = $cmd->getUser($_POST['employe']);
		    $userId = "";
		    $btnAction = "admin.php";
		} else {
		    $userId = $cmd->getUserLogin($_SESSION['username']);
		    $user = $cmd->getUser($userId['id_emp']);
		    $btnAction = "impression.php";
		}

		// Function qui va me formater les données reçu en heure , min et seconde
		function formatHeure($temps, $tempsRattrapage) {

		    $heure = 0;
		    $min = 0;
		    $seconde = 0;
		    // transform les minutes en heure
		    $format = $temps / 60;

		    if (is_float($tempsRattrapage)) {
			$tabRattrapage = explode(".", $tempsRattrapage);

			$min += $tabRattrapage[0];
			if (count($tabRattrapage) == 1) {
			    
			} else {
			    $seconde +=60 / (100 / ($tabRattrapage[1] * 10));
			}
		    }

		    if (is_float($format)) {
			$tab = explode(".", $format);
			$heure = $tab[0];
			$min += 60 / (100 / ($tab[1] * 10));
		    } else {
			$heure = $format;
			$min += 0;
		    }

		    if (is_float($seconde)) {
			$tab = explode(".", $seconde);
			$seconde = substr($tab[0], 0, 2);
		    }

		    if ($min == 0 && $seconde == 0) {
			return $heure . ":" . $min;
		    } else if ($min == 0 && $seconde != 0) {
			return $heure . ":" . $seconde . "''";
		    } else if ($min != 0 && $seconde == 0) {
			return $heure . ":" . $min . "'";
		    } else {
			return $heure . ":" . $min . "':" . $seconde . "''";
		    }
		}

		// Fonction qui va retourner le nombre de jours entiers
		function format_jourRattrapage($nbrJour, $temps) {

		    $jour = 0;
		    $tabRattrapage = explode(".", $nbrJour);
		    foreach ($tabRattrapage as $key => $value) {
			if ($key == 0) {
			    $jour = $value;
			}
		    }
		    return $jour;
		}

		$annee = 2015;
		$nbrJourFerieEntier = 0;
		$nbrJourRattrapageModif = 0;
		$nbrJourRattrapage = 0;
		$nbrJourWeek = 104;
		$jourAnnee = 0;
		$heureJournalierEntreprise = 0;
		$dureeJournalierMin = 0;
		$dureeJournalierMinPoste = 0;
		$nbrJourOuvrable = 0;
		$nbrJourtravail = 0;
		$dureeRattrapageMin = 0;
		$totalRattrapagejour = 0;

		if (date("L", mktime(0, 0, 0, 1, 1, $annee)) == 1) {
		    $jourAnnee = 366;
		} else {
		    $jourAnnee = 365;
		}
		// Tableau qui va contenir toutes les information de tous les employés
		$tabUser = $cmd->getAllUser();
		// Tableau qui va contenir toutes les informations sur les jours fériés de l'entreprise
		$tabJourFerie = $cmd->getJourFerie();
		//Tableau qui va contenir toutes les informations sur les jours de rattrapages de l'entreprise
		$tabJourRattrapage = $cmd->getJourRattrapage();
		// Tableau qui va contenir le nombre d'heure journalier a faire 
		$tabHeureJournalier = $cmd->getTempsTravailJournalier();
		// Boucle qui va calculer le nombre de jours féries
		foreach ($tabJourFerie as $key => $value) {
		    $nbrJourFerieEntier++;
		}
		// Boucle qui va calculer le nombre de jours de rattrapages
		foreach ($tabJourRattrapage as $key => $value) {
		    $nbrJourRattrapage ++;
		}

		// Variable qui va contenir le nombre de jours ouvrables dans une année
		$nbrJourOuvrable = ($jourAnnee - $nbrJourWeek) - $nbrJourFerieEntier;
		// Variable qui va contenir le nombre de temps a travaillé quotidiennement

		$tabHeureJournalier = $cmd->getTempsTravailJournalier();

		$heureJournalierEntreprise = $tabHeureJournalier['temps_travail_jour_ent'];

		// Tableau qui va contenir les heures et les minutes de travail journalier pour un poste a 100% de l'entreprise
		$tabTemps[0] = substr($heureJournalierEntreprise, 0, 2);
		$tabTemps[1] = substr($heureJournalierEntreprise, 2, 4);

		// Variable qui va contenir le nombre de min de travail journalier pour un poste a 100%
		$dureeJournalierMin = (($tabTemps[0] * 60) + $tabTemps[1]);

		/* Minipulation pour obtenir la duree de travail quotidienne*/
		
		$periode = $cmd->getPeriodeUser($user['id_pos']);
		$nbrJourRattrapageModif = $nbrJourRattrapage;
		$nbrJourRattrapageModif = ($nbrJourRattrapageModif / 100) * $periode['pourcentage_per'];
		$nbrJourtravail = $nbrJourOuvrable - $periode['vacances_per'] - $nbrJourRattrapageModif;
		$dureeJournalierMinPoste = ($dureeJournalierMin / 100) * $periode['pourcentage_per'] . "<br/>";

		$dureeRattrapageMin = $nbrJourRattrapageModif * 60;

		$rattrapage = format_jourRattrapage($nbrJourRattrapageModif, $heureJournalierEntreprise);
		$totalRattrapagejour = $dureeRattrapageMin / $nbrJourtravail;
		$resultat = formatHeure($dureeJournalierMinPoste, $totalRattrapagejour);
		$totalMinuteJournalier = round(($dureeJournalierMinPoste + $totalRattrapagejour), 2);

		// Si on passe par l'administrateur
		
		if (isset($_POST['apercuAdmin'])) {
		    $user = $cmd->getUser($_POST['employe']);
		    $userId = "";
		} else {
		    $userId = $cmd->getUserLogin($_SESSION['username']);
		    $user = $cmd->getUser($userId['id_emp']);
		}



		$idUser = $cmd->getUserLogin($_SESSION['username']);
		$tabRatrapage = $cmd->getJourRattrapage();
		$tabFerie = $cmd->getJourFerie();
		$tabFermeture = $cmd->getJourFermeture();
		$tabVacances = $cmd->getVacances($idUser['id_emp']);

		/* Remplit les tableaux avec les nombre de jours*/

		if (!empty($tabRatrapage)) {
		    foreach ($tabRatrapage as $key => $value) {
			$tabJourRattrapage[$key] = $value['date_jra'];
		    }
		} else {
		    $tabJourRattrapage[0] = "";
		}
		if (!empty($tabFerie)) {
		    foreach ($tabFerie as $key => $value) {
			$tabJourFerie[$key] = $value['date_jfe'];
		    }
		} else {
		    $tabJourFerie[0] = "";
		}
		if (!empty($tabFermeture)) {
		    foreach ($tabFermeture as $key => $value) {
			$tabJourFermetureDebut[$key] = $value['date_debut_fer'];
			$tabJourFermetureFin[$key] = $value['date_fin_fer'];
		    }
		} else {
		    $tabJourFermetureDebut[0] = "";
		    $tabJourFermetureFin[0] = "";
		}
		if (!empty($tabVacances)) {
		    foreach ($tabVacances as $key => $value) {
			$tabJourVacanceDebut[$key] = $value['date_debut_vac'];
			$tabJourVacanceFin[$key] = $value['date_fin_vac'];
		    }
		} else {
		    $tabJourVacanceDebut[0] = "";
		    $tabJourVacanceFin[0] = "";
		}

		
		$tabPlage = $cmd->getPlageUser($user['id_emp']);

		/* Remplit les tableaux*/
		if (!empty($tabPlage)) {
		    foreach ($tabPlage as $key => $value) {
			$tabDatePlage[$key] = $value['date_pla'];
			$tabTempsPlage[$key] = $value['total_temps_pla'];
		    }
		} else {
		    $tabDatePlage[0] = "";
		    $tabTempsPlage[0] = "";
		}

		
		/*Boucle qui va définir le nombre de temps effectuer*/
		for ($i = 1; $i <= 12; $i++) {
		    for ($y = 1; $y <= date("t", mktime(0, 0, 0, $y, 1, $annee)); $y++) {
			if (strlen($i) == 1) {
			    $i = "0" . $i;
			}if (strlen($y) == 1) {
			    $y = "0" . $y;
			}
			$valTempsTotal = 0;
			if ($tabDatePlage[0] == $annee . "-" . $i . "-" . $y && count($tabDatePlage) == 1) {
			    $valTempsTotal+=$tabTempsPlage[0];
			} else {
			    while ($tabDatePlage[0] == $annee . "-" . $i . "-" . $y) {
				if (count($tabDatePlage) == 1) {
				    $valTempsTotal+=$tabTempsPlage[0];
				    break;
				} else {
				    unset($tabDatePlage[0]);
				    $tabDatePlage = array_values($tabDatePlage);

				    $valTempsTotal+=$tabTempsPlage[0];
				    unset($tabTempsPlage[0]);
				    $tabTempsPlage = array_values($tabTempsPlage);
				}
			    }
			}
			$_SESSION[$i . "_total"]+=$valTempsTotal;
			$heurePlage = 0;
			$minutePlage = 0;
			while ($valTempsTotal >= 60) {
			    $heurePlage+=1;
			    $valTempsTotal = $valTempsTotal - 60;
			}
			$minutePlage = $valTempsTotal;

			$val = $heurePlage . ":" . $minutePlage;
			$_SESSION[$i . "-" . $y] = $val;
		    }
		}
		if ($_POST['mois'] == 13) {
		    $periode = $cmd->getPeriodeUser($user['id_pos']);
		    $tabJourFerieNbr = $cmd->getJourFerie();
		    $nbrJourFerieEntier = 0;
		    $nbrJourFerieDemi = 0;

		    foreach ($tabJourFerieNbr as $key => $value) {
			if ($value['duree_jfe'] == 1) {
			    $nbrJourFerieEntier++;
			} else {
			    $nbrJourFerieDemi++;
			}
		    }

		    echo"Nom :" . $user['nom_emp'] . "<br/>";
		    echo"Prenom :" . $user['prenom_emp'] . "<br/>";
		    echo"Année:" . $annee . "<br/>";

		    echo"Nb heure/jour :$resultat<br/>";
		    echo"Pourcentage travail :" . $periode['pourcentage_per'] . "%<br/>";
		    echo"Nb jour Férie:" . $nbrJourFerieEntier . "<br/>";
		    echo"Nb jours vacacnes:" . $periode['vacances_per'] . "<br/>";
		    echo"Nb demi-jour férie:" . $nbrJourFerieDemi . "<br/>";
		    $nbrFeries = 0;
		    $nbrVacances = 0;
		    $nbrRattrapages = 0;
		    $nbrFermetures = 0;
		    echo"<div id='tableau2'>";
		    echo"<table border='1px'>";
		    echo"<tr><td></td><td>VACANCES</td><td>RATTRAPAGES</td><td>FÉRIÉS</td><td>BONUS/MALUS</td>";
		    $totalJour = 0;
		    for ($mois = 1; $mois <= 12; $mois++) {
			$nbrFeries = 0;
			$nbrVacances = 0;
			$nbrRattrapages = 0;
			$totalJour = 0;

			echo"<tr>";
			echo"<td>$tab_mois_nom[$mois]</td>";
			for ($jour = 1; $jour <= date("t", mktime(0, 0, 0, $mois, 1, $annee)); $jour++) {
			    $valJour = date("N", mktime(0, 0, 0, $mois, $jour, $annee));
			    if ($valJour <= 5) {
				$totalJour+=1;
			    }
			    if (strlen($jour) == 1) {
				$jourText = "0" . $jour;
			    } else {
				$jourText = $jour;
			    }
			    if (strlen($mois) == 1) {
				$moisText = "0" . $mois;
			    }

			    if ($annee . "-" . $moisText . "-" . $jourText == $tabJourRattrapage[0]) {
				$nbrIndiceRattrapage = count($tabJourRattrapage);
				if ($nbrIndiceRattrapage == 1) {
				    
				} else {
				    unset($tabJourRattrapage[0]);
				    $tabJourRattrapage = array_values($tabJourRattrapage);
				}
				$nbrRattrapages+=1;
				$totalJour-=1;
			    } else if ($annee . "-" . $moisText . "-" . $jourText == $tabJourFerie[0]) {
				$nbrIndiceFerie = count($tabJourFerie);
				if ($nbrIndiceFerie == 1) {
				    
				} else {
				    unset($tabJourFerie[0]);
				    $tabJourFerie = array_values($tabJourFerie);
				}
				$nbrFeries+=1;
				$totalJour-=1;
			    } else if ($annee . "-" . $moisText . "-" . $jourText >= $tabJourFermetureDebut[0] && $annee . "-" . $moisText . "-" . $jourText <= $tabJourFermetureFin[0]) {
				if ($annee . "-" . $moisText . "-" . $jourText == $tabJourFermetureFin[0]) {
				    $nbrIndiceFermetureDebut = count($tabJourFermetureDebut);
				    if ($nbrIndiceFermetureDebut == 1) {
					
				    } else {
					unset($tabJourFermetureDebut[0]);
					unset($tabJourFermetureFin[0]);
					$tabJourFermetureDebut = array_values($tabJourFermetureDebut);
					$tabJourFermetureFin = array_values($tabJourFermetureFin);
				    }
				}
				$nbrVacances+=1;
				$totalJour-=1;
			    } else if ($annee . "-" . $moisText . "-" . $jourText >= $tabJourVacanceDebut[0] && $annee . "-" . $moisText . "-" . $jourText <= $tabJourVacanceFin[0]) {
				if ($annee . "-" . $moisText . "-" . $jourText == $tabJourVacanceFin[0]) {
				    $nbrIndiceVacanceDebut = count($tabJourVacanceDebut);
				    if ($nbrIndiceVacanceDebut == 1) {
					
				    } else {
					unset($tabJourVacanceDebut[0]);
					unset($tabJourVacanceFin[0]);
					$tabJourVacanceDebut = array_values($tabJourVacanceDebut);
					$tabJourVacanceFin = array_values($tabJourVacanceFin);
				    }
				}
				$nbrVacances+=1;
				$totalJour-=1;
			    }
			}

			$bunusMalus = 0;
			$bonusMalus = (($totalJour * $totalMinuteJournalier) - $_SESSION[$moisText . "_total"] );
			if (($totalJour * $totalMinuteJournalier) > $_SESSION[$moisText . "_total"]) {
			    $signe = "-";
			} else {
			    $signe = "+";
			}
			$heureBonusMalus = 0;
			$minuteBonusMalus = 0;
			while ($bonusMalus >= 60) {
			    $heureBonusMalus+=1;
			    $bonusMalus = $bonusMalus - 60;
			}
			$minuteBonusMalus = round($bonusMalus);
			$heurePlage = 0;
			$minutePlage = 0;
			while ($_SESSION[$moisText . "_total"] >= 60) {
			    $heurePlage+=1;
			    $_SESSION[$moisText . "_total"] = $_SESSION[$moisText . "_total"] - 60;
			}


			$minutePlage = $_SESSION[$moisText . "_total"];


			echo"<td>$nbrVacances</td><td>$nbrRattrapages</td><td>$nbrFeries</td><td>" . $signe . "" . $heureBonusMalus . ":" . $minuteBonusMalus . "</td></tr>";
		    }
		    echo"</table>";
		    echo"<div id='noimprime'>";


		    echo"<form action ='admin.php' method = 'post' />";
		    echo" <input id = 'impression' name = 'impression' type = 'button' class='btn' onclick = 'imprimer_page()' value = 'IMPRESSION' />";
		    echo"<div id='btnRetour3'><input type = 'submit' name = 'retour' class='btn' value = 'RETOUR'/></div>";
		    echo"</form>";
		    echo"</div>";
		    echo"</form>";
		} else {
		    echo"<div id='nomSimple'>" . $user['nom_emp'] . "   " . $user['prenom_emp'] . "</div>";

		    echo"<div id='noimprime'>";


		    echo"<form action ='$btnAction' method = 'post' />";
		    echo" <input id = 'impression' name = 'impression' type = 'button' class='btn' onclick = 'imprimer_page()' value = 'IMPRESSION' />";
		    echo"<div id='btnRetour3'><input type = 'submit' name = 'retour' class='btn' value = 'RETOUR'/></div>";
		    echo"</form>";
		    echo"</div>";
		    echo"</form>";
		    echo"</div>";
		    $tabPlage = $cmd->getPlageUser($user['id_emp']);
		    //  print_r($tabPlage);

		    foreach ($tabPlage as $key => $value) {
			$tabDatePlage[$key] = $value['date_pla'];
			$tabTempsPlage[$key] = $value['total_temps_pla'];
		    }





		    echo"<div id='tableau'>";
		    echo"<table border='1'>";
		    echo"<tr><td>CODE</td><td>JOUR</td><td>TEMPS EFFECTUER</td></tr>";
		    $totalJour = 0;

		    for ($jour = 1; $jour <= date("t", mktime(0, 0, 0, $_POST['mois'], 1, $annee)); $jour++) {
			$cote = "";

			$valJour = date("N", mktime(0, 0, 0, $_POST['mois'], $jour, $annee));
			if ($valJour <= 5) {
			    $totalJour+=1;
			}

			// Switch : pour determiner le jour de la semaine
			switch ($valJour) {
			    case 1:
				$valJour = "lundi";
				break;
			    case 2:
				$valJour = "mardi";
				break;
			    case 3:
				$valJour = "mercredi";
				break;
			    case 4:
				$valJour = "jeudi";
				break;
			    case 5:
				$valJour = "vendredi";
				break;
			    case 6:
				$valJour = "samedi";
				break;
			    case 7:
				$valJour = "dimanche";
			    default :
			}

			if (strlen($jour) == 1) {
			    $jourText = "0" . $jour;
			} else {
			    $jourText = $jour;
			}
			if (strlen($_POST['mois']) == 1) {
			    $moisText = "0" . $_POST['mois'];
			}

			if ($annee . "-" . $moisText . "-" . $jourText == $tabJourRattrapage[0]) {
			    $nbrIndiceRattrapage = count($tabJourRattrapage);
			    if ($nbrIndiceRattrapage == 1) {
				
			    } else {
				unset($tabJourRattrapage[0]);
				$tabJourRattrapage = array_values($tabJourRattrapage);
			    }
			    $totalJour-=1;
			    $cote = "R";
			} else if ($annee . "-" . $moisText . "-" . $jourText == $tabJourFerie[0]) {
			    $nbrIndiceFerie = count($tabJourFerie);
			    if ($nbrIndiceFerie == 1) {
				
			    } else {
				unset($tabJourFerie[0]);
				$tabJourFerie = array_values($tabJourFerie);
			    }
			    $totalJour-=1;
			    $cote = "F";
			} else if ($annee . "-" . $moisText . "-" . $jourText >= $tabJourFermetureDebut[0] && $annee . "-" . $moisText . "-" . $jourText <= $tabJourFermetureFin[0]) {
			    if ($annee . "-" . $moisText . "-" . $jourText == $tabJourFermetureFin[0]) {
				$nbrIndiceFermetureDebut = count($tabJourFermetureDebut);
				if ($nbrIndiceFermetureDebut == 1) {
				    
				} else {
				    unset($tabJourFermetureDebut[0]);
				    unset($tabJourFermetureFin[0]);
				    $tabJourFermetureDebut = array_values($tabJourFermetureDebut);
				    $tabJourFermetureFin = array_values($tabJourFermetureFin);
				}
			    }
			    $cptControl = 1;
			    $cote = "V";
			} else if ($annee . "-" . $moisText . "-" . $jourText >= $tabJourVacanceDebut[0] && $annee . "-" . $moisText . "-" . $jourText <= $tabJourVacanceFin[0]) {
			    if ($annee . "-" . $moisText . "-" . $jourText == $tabJourVacanceFin[0]) {
				$nbrIndiceVacanceDebut = count($tabJourVacanceDebut);
				if ($nbrIndiceVacanceDebut == 1) {
				    
				} else {
				    unset($tabJourVacanceDebut[0]);
				    unset($tabJourVacanceFin[0]);
				    $tabJourVacanceDebut = array_values($tabJourVacanceDebut);
				    $tabJourVacanceFin = array_values($tabJourVacanceFin);
				}
			    }
			    $totalJour-=1;
			    $cote = "V";
			}


			echo"<tr><td>$cote</td><td>$valJour-$jourText  " . $tab_mois_nom[$_POST['mois']] . " </td><td> " . $_SESSION[$moisText . "-" . $jourText] . "</td></tr>";
		    }
		    $bunusMalus = 0;
		    $bonusMalus = (($totalJour * $totalMinuteJournalier) - $_SESSION[$moisText . "_total"] );
		    if (($totalJour * $totalMinuteJournalier) > $_SESSION[$moisText . "_total"]) {
			$signe = "-";
		    } else {
			$signe = "+";
		    }
		    $heureBonusMalus = 0;
		    $minuteBonusMalus = 0;
		    while ($bonusMalus >= 60) {
			$heureBonusMalus+=1;
			$bonusMalus = $bonusMalus - 60;
		    }
		    $minuteBonusMalus = round($bonusMalus);
		    $heurePlage = 0;
		    $minutePlage = 0;
		    while ($_SESSION[$moisText . "_total"] >= 60) {
			$heurePlage+=1;
			$_SESSION[$moisText . "_total"] = $_SESSION[$moisText . "_total"] - 60;
		    }
		    $minutePlage = $_SESSION[$moisText . "_total"];

		    echo"<tr><td>TOTAL </td><td>$totalJour</td><td>" . $heurePlage . ":" . $minutePlage . "</td></tr>";
		    echo"<tr><td></td><td>BONUS/MALUS</td><td>" . $signe . "" . $heureBonusMalus . " : " . $minuteBonusMalus . "</td></tr>";
		    echo"</table>";
		    echo"</div>";




		    echo"<div id='signature1'>SIGNATURE EMPLOYÉ</div>";
		    echo"<div id='signature2'>SIGNATURE DIRECTION</div>";
		    echo"<div id='signature3'>SIGNATURE RH</div>";
		    echo"</div>";
		}
	    } else {
		echo"<div id='content'>";
		echo"<div id='titre1'>CHOIX DE L'IMPRESSION</div>";

		echo"<form action = 'impression.php' method = 'post' />";
		echo"<div id='text1'>CHOISIR LE MOIS</div>";
		echo"<div id='mois'><select name='mois'>";
		// Boucle qui va parcourir le tableau

		for ($i = 1; $i <= 13; $i++) {
		    echo"<option value=$i>$tab_mois_nom[$i]</option>";
		}
		echo "</select></div>";

		echo"<div id='btnApercu'><input type = 'submit' class='btn' name = 'apercu' value = 'VALIDER'/></div>";
		echo"</form>";

		echo"<form action = 'employe.php' method = 'post' />";
		echo"<div id='btnRetour'><input type = 'submit' name = 'retour' class='btn' value = 'RETOUR'/></div>";
		echo"</form>";
		echo"<div>";
	    }
	}
	?>

        <script type="text/javascript">
	    function imprimer_page() {

		window.print();
	    }
        </script>
    </body>
</html>
