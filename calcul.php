<?php
session_start();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/calcul.css" media="all"/>
        <title></title>
    </head>
    <body>
        <?php
        require("./../config/time_gest/config.cfg.php");
        require ("./Class/inc.class.php");
        require ("./Class/function.class.php");
        $pdo = new Mypdo();
        $cmd = new Projet($pdo);
	
	// Initialisation des variables
        $annee = 2015;
        $nbrJourFerieEntier = 0;
        $nbrJourRattrapageModif = 0;
        $nbrJourRattrapage = 0;
        $nbrJourWeek = 0;
        $jourAnnee = 0;
        $heureJournalierEntreprise = 0;
        $dureeJournalierMin = 0;
        $dureeJournalierMinPoste = 0;
        $nbrJourOuvrable = 0;
        $nbrJourtravail = 0;
        $dureeRattrapageMin = 0;
        $totalRattrapagejour = 0;

        // Function qui va me formater les données reçu en heure , min et seconde
        function formatHeure($temps, $tempsRattrapage) {

            $heure = 0;
            $min = 0;
            $seconde = 0;
            // transforme les minutes en heure
            $format = $temps / 60;

            if (is_float($tempsRattrapage)) {
                $tabRattrapage = explode(".", $tempsRattrapage);

                $min += $tabRattrapage[0];
                if (count($tabRattrapage) == 1) {
                    
                } else {
                    $seconde +=60 / (100 / ($tabRattrapage[1] * 10));
                }
            }

            if (is_float($format)) {
                $tab = explode(".", $format);
                $heure = $tab[0];
                $min += 60 / (100 / ($tab[1] * 10));
            } else {
                $heure = $format;
                $min += 0;
            }

            if (is_float($seconde)) {
                $tab = explode(".", $seconde);
                $seconde = substr($tab[0], 0, 2);
            }

            if ($min == 0 && $seconde == 0) {
                return $heure . ":" . $min;
            } else if ($min == 0 && $seconde != 0) {
                return $heure . ":" . $seconde . "''";
            } else if ($min != 0 && $seconde == 0) {
                  return $heure . ":" . $min."'";
            } else {
                return $heure . ":" . $min . "':" . $seconde . "''";
            }
        }

	// Fonction qui va retourner le nombre de jours entiers
        function format_jourRattrapage($nbrJour, $temps) {

            $jour = 0;
            $tabRattrapage = explode(".", $nbrJour);
            foreach ($tabRattrapage as $key => $value) {
                if ($key == 0) {
                    $jour = $value;
                }
            }
            return $jour;
        }

        // Condition pour savoir si c'est une anneé bissextile
        if (date("L", mktime(0, 0, 0, 1, 1, $annee)) == 1) {
            $jourAnnee = 366;
        } else {
            $jourAnnee = 365;
        }
        // Boucle qui va passer en revue une année
        for ($mois = 1; $mois <= 12; $mois++) {
            // Bo ucleu qui va passer en revue tous les ours de l'année afin de pouvoir determiné si le jours est un jours de week-end ou non
            for ($jour = 1; $jour <= date("t", mktime(0, 0, 0, $mois, 1, $annee)); $jour++) {
                if (date("N ", mktime(0, 0, 0, $mois, $jour, $annee)) > 5) {
                    // Si c'est un jour de week-end alors la varible sera incrémente de 1
                    $nbrJourWeek++;
                }
            }
        }
        // Tableau qui va contenir toutes les information de tous les employés
        $tabUser = $cmd->getAllUser();
        // Tableau qui va contenir toutes les informations sur les jours fériés de l'entreprise
        $tabJourFerie = $cmd->getJourFerie();
        //Tableau qui va contenir toutes les informations sur les jours de rattrapages de l'entreprise
        $tabJourRattrapage = $cmd->getJourRattrapage();
        // Tableau qui va contenir le nombre d'heure journalier a faire 
        $tabHeureJournalier = $cmd->getTempsTravailJournalier();
        // Boucle qui va calculer le nombre de jours féries
        foreach ($tabJourFerie as $key => $value) {
            $nbrJourFerieEntier++;
        }
        // Boucle qui va calculer le nombre de jours de rattrapages
        foreach ($tabJourRattrapage as $key => $value) {
            $nbrJourRattrapage ++;
        }

        // Variable qui va contenir le nombre de jours ouvrables dans une année
        $nbrJourOuvrable = ($jourAnnee - $nbrJourWeek) - $nbrJourFerieEntier;
        // Variable qui va contenir le nombre de temps a travaillé quotidiennement
        $heureJournalierEntreprise = $tabHeureJournalier['temps_travail_jour_ent'];

        // Tableau qui va contenir les heures et les minutes de travail journalier pour un poste a 100% de l'entreprise
        $tabTemps[0] = substr($heureJournalierEntreprise, 0, 2);
        $tabTemps[1] = substr($heureJournalierEntreprise, 2, 4);

        // Variable qui va contenir le nombre de min de travail journalier pour un poste a 100%
        $dureeJournalierMin = (($tabTemps[0] * 60) + $tabTemps[1]);

        echo"<div id='content'>";
        echo"<table border='1'>";
        echo"<tr><td>Employé</td><td>%</td><td>Vacances (jours)</td><td>Jours non travaillés</td><td>Jours fériés</td><td>Durée du travail quotidien</td></tr>";


        // Boucle qui va afficher le tableau résultat
        foreach ($tabUser as $key => $value) {

            // Variab le qui va contenir les informations de l'utilisateur
            $user = $cmd->getUser($value['id_emp']);

            $periode = $cmd->getPeriodeUser($user['id_pos']);
            $nbrJourRattrapageModif = $nbrJourRattrapage;
            $nbrJourRattrapageModif = ($nbrJourRattrapageModif / 100) * $periode['pourcentage_per'];
            $nbrJourtravail = $nbrJourOuvrable - $periode['vacances_per'] - $nbrJourRattrapageModif;
            $dureeJournalierMinPoste = ($dureeJournalierMin / 100) * $periode['pourcentage_per'] . "<br/>";

            $dureeRattrapageMin = $nbrJourRattrapageModif * 60;

            $rattrapage = format_jourRattrapage($nbrJourRattrapageModif, $heureJournalierEntreprise);
            $totalRattrapagejour = $dureeRattrapageMin / $nbrJourtravail;
            $resultat = formatHeure($dureeJournalierMinPoste, $totalRattrapagejour);

            $_SESSION['id_' . $value['id_emp']] = $resultat;
            echo"<tr><td>" . $value['nom_emp'] . " " . $value['prenom_emp'] . "</td><td>" . $periode['pourcentage_per'] . "%</td><td>" . $periode['vacances_per'] . "</td>"
            . "<td>$rattrapage</td><td>$nbrJourFerieEntier</td><td>$resultat</td></tr>";
        }

        echo"</table>";
        
        echo"<form action='admin.php' method='post' >";
        echo"<div id='btnRetour'><input type='submit' name='retour' class='btn' value='RETOUR'/></div>";
        echo"</form>";
        echo"</div>";
        ?>
    </body>
</html>
